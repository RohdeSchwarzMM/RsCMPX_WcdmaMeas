WcdmaMeas
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.WcdmaMeasCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval.rst
	WcdmaMeas_OlpControl.rst
	WcdmaMeas_OoSync.rst
	WcdmaMeas_Prach.rst
	WcdmaMeas_Tpc.rst