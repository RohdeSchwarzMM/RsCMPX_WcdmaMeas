Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:PRACh:TRACe:EVMagnitude:PEAK:CURRent
	single: READ:WCDMa:MEASurement<instance>:PRACh:TRACe:EVMagnitude:PEAK:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:PRACh:TRACe:EVMagnitude:PEAK:CURRent
	READ:WCDMa:MEASurement<instance>:PRACh:TRACe:EVMagnitude:PEAK:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Prach.Trace.EvMagnitude.Peak.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: