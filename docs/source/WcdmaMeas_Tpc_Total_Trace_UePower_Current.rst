Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:TPC:TOTal:TRACe:UEPower:CURRent
	single: FETCh:WCDMa:MEASurement<instance>:TPC:TOTal:TRACe:UEPower:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:TPC:TOTal:TRACe:UEPower:CURRent
	FETCh:WCDMa:MEASurement<instance>:TPC:TOTal:TRACe:UEPower:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Tpc.Total.Trace.UePower.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: