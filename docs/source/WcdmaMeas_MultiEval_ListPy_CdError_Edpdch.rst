Edpdch<EdpdChannel>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.wcdmaMeas.multiEval.listPy.cdError.edpdch.repcap_edpdChannel_get()
	driver.wcdmaMeas.multiEval.listPy.cdError.edpdch.repcap_edpdChannel_set(repcap.EdpdChannel.Nr1)





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.CdError.Edpdch.EdpdchCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.listPy.cdError.edpdch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_ListPy_CdError_Edpdch_Average.rst
	WcdmaMeas_MultiEval_ListPy_CdError_Edpdch_Current.rst
	WcdmaMeas_MultiEval_ListPy_CdError_Edpdch_Maximum.rst
	WcdmaMeas_MultiEval_ListPy_CdError_Edpdch_StandardDev.rst