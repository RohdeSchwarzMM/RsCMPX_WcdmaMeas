CdPower
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.Segment.CdPower.CdPowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.listPy.segment.cdPower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_ListPy_Segment_CdPower_Average.rst
	WcdmaMeas_MultiEval_ListPy_Segment_CdPower_Current.rst
	WcdmaMeas_MultiEval_ListPy_Segment_CdPower_Maximum.rst
	WcdmaMeas_MultiEval_ListPy_Segment_CdPower_Minimum.rst
	WcdmaMeas_MultiEval_ListPy_Segment_CdPower_StandardDev.rst