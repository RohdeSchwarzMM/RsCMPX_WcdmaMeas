MaxPower
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:TPC:LIMit:ILPControl:MAXPower:URPClass
	single: CONFigure:WCDMa:MEASurement<instance>:TPC:LIMit:ILPControl:MAXPower
	single: CONFigure:WCDMa:MEASurement<instance>:TPC:LIMit:ILPControl:MAXPower:ACTive

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:TPC:LIMit:ILPControl:MAXPower:URPClass
	CONFigure:WCDMa:MEASurement<instance>:TPC:LIMit:ILPControl:MAXPower
	CONFigure:WCDMa:MEASurement<instance>:TPC:LIMit:ILPControl:MAXPower:ACTive



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Tpc.Limit.IlpControl.MaxPower.MaxPowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wcdmaMeas.tpc.limit.ilpControl.maxPower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WcdmaMeas_Tpc_Limit_IlpControl_MaxPower_UserDefined.rst