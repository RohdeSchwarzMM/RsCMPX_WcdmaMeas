Chip
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.Merror.Chip.ChipCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.trace.merror.chip.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_Trace_Merror_Chip_Average.rst
	WcdmaMeas_MultiEval_Trace_Merror_Chip_Current.rst
	WcdmaMeas_MultiEval_Trace_Merror_Chip_Maximum.rst