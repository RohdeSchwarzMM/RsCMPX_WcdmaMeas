IlpControl
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Tpc.Limit.IlpControl.IlpControlCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wcdmaMeas.tpc.limit.ilpControl.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WcdmaMeas_Tpc_Limit_IlpControl_EpStep.rst
	Configure_WcdmaMeas_Tpc_Limit_IlpControl_MaxPower.rst
	Configure_WcdmaMeas_Tpc_Limit_IlpControl_MinPower.rst
	Configure_WcdmaMeas_Tpc_Limit_IlpControl_PsGroup.rst
	Configure_WcdmaMeas_Tpc_Limit_IlpControl_Pstep.rst