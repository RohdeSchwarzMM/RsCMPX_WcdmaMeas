Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:PRACh:TRACe:PERRor[:RMS]:CURRent
	single: READ:WCDMa:MEASurement<instance>:PRACh:TRACe:PERRor[:RMS]:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:PRACh:TRACe:PERRor[:RMS]:CURRent
	READ:WCDMa:MEASurement<instance>:PRACh:TRACe:PERRor[:RMS]:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Prach.Trace.Perror.Rms.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: