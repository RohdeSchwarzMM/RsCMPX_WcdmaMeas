Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:KFILter:MAXimum
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:KFILter:MAXimum

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:KFILter:MAXimum
	FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:KFILter:MAXimum



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.Emask.Kfilter.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: