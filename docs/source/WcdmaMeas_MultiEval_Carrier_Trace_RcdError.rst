RcdError
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.RcdError.RcdErrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.carrier.trace.rcdError.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_Carrier_Trace_RcdError_Dpcch.rst
	WcdmaMeas_MultiEval_Carrier_Trace_RcdError_Dpdch.rst
	WcdmaMeas_MultiEval_Carrier_Trace_RcdError_Edpcch.rst
	WcdmaMeas_MultiEval_Carrier_Trace_RcdError_Edpdch.rst
	WcdmaMeas_MultiEval_Carrier_Trace_RcdError_Hsdpcch.rst
	WcdmaMeas_MultiEval_Carrier_Trace_RcdError_Sf.rst