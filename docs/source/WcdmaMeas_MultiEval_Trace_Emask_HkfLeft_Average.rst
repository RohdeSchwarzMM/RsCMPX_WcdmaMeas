Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:HKFLeft:AVERage
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:HKFLeft:AVERage

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:HKFLeft:AVERage
	FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:HKFLeft:AVERage



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.Emask.HkfLeft.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: