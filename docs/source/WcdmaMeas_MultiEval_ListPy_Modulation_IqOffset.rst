IqOffset
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.Modulation.IqOffset.IqOffsetCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.listPy.modulation.iqOffset.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_ListPy_Modulation_IqOffset_Average.rst
	WcdmaMeas_MultiEval_ListPy_Modulation_IqOffset_Current.rst
	WcdmaMeas_MultiEval_ListPy_Modulation_IqOffset_Maximum.rst
	WcdmaMeas_MultiEval_ListPy_Modulation_IqOffset_StandardDev.rst