Hsdpcch
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.CdError.Hsdpcch.HsdpcchCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.listPy.cdError.hsdpcch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_ListPy_CdError_Hsdpcch_Average.rst
	WcdmaMeas_MultiEval_ListPy_CdError_Hsdpcch_Current.rst
	WcdmaMeas_MultiEval_ListPy_CdError_Hsdpcch_Maximum.rst
	WcdmaMeas_MultiEval_ListPy_CdError_Hsdpcch_StandardDev.rst