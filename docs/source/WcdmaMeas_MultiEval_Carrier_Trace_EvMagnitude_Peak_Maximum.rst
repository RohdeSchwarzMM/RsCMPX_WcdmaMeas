Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:EVMagnitude:PEAK:MAXimum
	single: READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:EVMagnitude:PEAK:MAXimum

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:EVMagnitude:PEAK:MAXimum
	READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:EVMagnitude:PEAK:MAXimum



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.EvMagnitude.Peak.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: