Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:MFLeft:CURRent
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:MFLeft:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:MFLeft:CURRent
	FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:MFLeft:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.Emask.MfLeft.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: