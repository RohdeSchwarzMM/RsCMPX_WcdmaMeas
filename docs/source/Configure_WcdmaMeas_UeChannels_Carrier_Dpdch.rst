Dpdch
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:UECHannels:CARRier<carrier>:DPDCh

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:UECHannels:CARRier<carrier>:DPDCh



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.UeChannels.Carrier.Dpdch.DpdchCls
	:members:
	:undoc-members:
	:noindex: