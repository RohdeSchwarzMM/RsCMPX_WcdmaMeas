Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:RCDerror:CURRent
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:RCDerror:CURRent
	single: READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:RCDerror:CURRent

.. code-block:: python

	CALCulate:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:RCDerror:CURRent
	FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:RCDerror:CURRent
	READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:RCDerror:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.RcdError.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: