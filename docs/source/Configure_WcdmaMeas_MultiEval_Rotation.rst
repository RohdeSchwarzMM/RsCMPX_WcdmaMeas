Rotation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:ROTation:MODulation

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:MEValuation:ROTation:MODulation



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.Rotation.RotationCls
	:members:
	:undoc-members:
	:noindex: