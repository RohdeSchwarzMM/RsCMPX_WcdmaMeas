Tsef
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:TPC:ILPControl:TSEF

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:TPC:ILPControl:TSEF



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Tpc.IlpControl.Tsef.TsefCls
	:members:
	:undoc-members:
	:noindex: