UePower
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.UePower.UePowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.carrier.trace.uePower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_Carrier_Trace_UePower_Average.rst
	WcdmaMeas_MultiEval_Carrier_Trace_UePower_Current.rst
	WcdmaMeas_MultiEval_Carrier_Trace_UePower_Maximum.rst
	WcdmaMeas_MultiEval_Carrier_Trace_UePower_Minimum.rst
	WcdmaMeas_MultiEval_Carrier_Trace_UePower_StandardDev.rst