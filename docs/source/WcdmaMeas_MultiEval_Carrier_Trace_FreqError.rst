FreqError
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.FreqError.FreqErrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.carrier.trace.freqError.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_Carrier_Trace_FreqError_Average.rst
	WcdmaMeas_MultiEval_Carrier_Trace_FreqError_Current.rst
	WcdmaMeas_MultiEval_Carrier_Trace_FreqError_Maximum.rst
	WcdmaMeas_MultiEval_Carrier_Trace_FreqError_StandardDev.rst