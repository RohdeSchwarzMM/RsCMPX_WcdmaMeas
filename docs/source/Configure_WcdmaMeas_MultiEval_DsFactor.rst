DsFactor
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:DSFactor:MODulation

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:MEValuation:DSFactor:MODulation



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.DsFactor.DsFactorCls
	:members:
	:undoc-members:
	:noindex: