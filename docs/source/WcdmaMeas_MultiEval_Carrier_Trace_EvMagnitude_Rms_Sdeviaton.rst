Sdeviaton
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:EVMagnitude[:RMS]:SDEViaton

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:EVMagnitude[:RMS]:SDEViaton



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.EvMagnitude.Rms.Sdeviaton.SdeviatonCls
	:members:
	:undoc-members:
	:noindex: