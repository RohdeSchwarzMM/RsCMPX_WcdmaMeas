Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:HKFLeft:CURRent
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:HKFLeft:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:HKFLeft:CURRent
	FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:HKFLeft:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.Emask.HkfLeft.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: