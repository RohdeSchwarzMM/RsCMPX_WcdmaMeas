Peak
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.EvMagnitude.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.carrier.trace.evMagnitude.peak.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_Carrier_Trace_EvMagnitude_Peak_Average.rst
	WcdmaMeas_MultiEval_Carrier_Trace_EvMagnitude_Peak_Current.rst
	WcdmaMeas_MultiEval_Carrier_Trace_EvMagnitude_Peak_Maximum.rst
	WcdmaMeas_MultiEval_Carrier_Trace_EvMagnitude_Peak_Sdeviaton.rst
	WcdmaMeas_MultiEval_Carrier_Trace_EvMagnitude_Peak_StandardDev.rst