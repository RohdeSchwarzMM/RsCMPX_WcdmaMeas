Current
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:LIST:MODulation:EVM:PEAK:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:LIST:MODulation:EVM:PEAK:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.Modulation.Evm.Peak.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: