Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:WCDMa:MEASurement<instance>:MEValuation:SPECtrum:CURRent
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:SPECtrum:CURRent
	single: READ:WCDMa:MEASurement<instance>:MEValuation:SPECtrum:CURRent

.. code-block:: python

	CALCulate:WCDMa:MEASurement<instance>:MEValuation:SPECtrum:CURRent
	FETCh:WCDMa:MEASurement<instance>:MEValuation:SPECtrum:CURRent
	READ:WCDMa:MEASurement<instance>:MEValuation:SPECtrum:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Spectrum.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: