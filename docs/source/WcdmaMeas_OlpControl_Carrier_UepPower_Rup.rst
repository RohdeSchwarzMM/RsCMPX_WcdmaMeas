Rup<RampUpCarrier>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr32
	rc = driver.wcdmaMeas.olpControl.carrier.uepPower.rup.repcap_rampUpCarrier_get()
	driver.wcdmaMeas.olpControl.carrier.uepPower.rup.repcap_rampUpCarrier_set(repcap.RampUpCarrier.Nr1)



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:OLPControl:CARRier<carrier>:UEPPower:RUP<rupcarrier>
	single: FETCh:WCDMa:MEASurement<instance>:OLPControl:CARRier<carrier>:UEPPower:RUP<rupcarrier>

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:OLPControl:CARRier<carrier>:UEPPower:RUP<rupcarrier>
	FETCh:WCDMa:MEASurement<instance>:OLPControl:CARRier<carrier>:UEPPower:RUP<rupcarrier>



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.OlpControl.Carrier.UepPower.Rup.RupCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.olpControl.carrier.uepPower.rup.clone()