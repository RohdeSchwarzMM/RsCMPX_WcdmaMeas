Cidx
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:CIDX

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:CIDX



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.ListPy.Segment.Cidx.CidxCls
	:members:
	:undoc-members:
	:noindex: