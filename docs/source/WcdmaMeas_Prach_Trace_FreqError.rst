FreqError
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Prach.Trace.FreqError.FreqErrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.prach.trace.freqError.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_Prach_Trace_FreqError_Current.rst