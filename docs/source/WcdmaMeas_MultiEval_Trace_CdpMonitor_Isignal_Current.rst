Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:CDPMonitor:ISIGnal:CURRent
	single: READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:CDPMonitor:ISIGnal:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:CDPMonitor:ISIGnal:CURRent
	READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:CDPMonitor:ISIGnal:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.CdpMonitor.Isignal.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: