IqImbalance
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.Modulation.IqImbalance.IqImbalanceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.listPy.modulation.iqImbalance.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_ListPy_Modulation_IqImbalance_Average.rst
	WcdmaMeas_MultiEval_ListPy_Modulation_IqImbalance_Current.rst
	WcdmaMeas_MultiEval_ListPy_Modulation_IqImbalance_Maximum.rst
	WcdmaMeas_MultiEval_ListPy_Modulation_IqImbalance_StandardDev.rst