Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:MERRor:CHIP:MAXimum
	single: READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:MERRor:CHIP:MAXimum

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:MERRor:CHIP:MAXimum
	READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:MERRor:CHIP:MAXimum



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.Merror.Chip.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: