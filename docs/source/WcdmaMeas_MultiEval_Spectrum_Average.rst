Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:WCDMa:MEASurement<instance>:MEValuation:SPECtrum:AVERage
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:SPECtrum:AVERage
	single: READ:WCDMa:MEASurement<instance>:MEValuation:SPECtrum:AVERage

.. code-block:: python

	CALCulate:WCDMa:MEASurement<instance>:MEValuation:SPECtrum:AVERage
	FETCh:WCDMa:MEASurement<instance>:MEValuation:SPECtrum:AVERage
	READ:WCDMa:MEASurement<instance>:MEValuation:SPECtrum:AVERage



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Spectrum.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: