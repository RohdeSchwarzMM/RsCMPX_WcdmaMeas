Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:CDPMonitor:QSIGnal:CURRent
	single: READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:CDPMonitor:QSIGnal:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:CDPMonitor:QSIGnal:CURRent
	READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:CDPMonitor:QSIGnal:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.CdpMonitor.Qsignal.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: