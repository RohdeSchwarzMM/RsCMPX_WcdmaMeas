Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:TRACe:UEPower:CURRent
	single: READ:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:TRACe:UEPower:CURRent
	single: CALCulate:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:TRACe:UEPower:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:TRACe:UEPower:CURRent
	READ:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:TRACe:UEPower:CURRent
	CALCulate:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:TRACe:UEPower:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Tpc.Carrier.Trace.UePower.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: