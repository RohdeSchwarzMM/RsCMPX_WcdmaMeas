Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:MFRight:MAXimum
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:MFRight:MAXimum

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:MFRight:MAXimum
	FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:MFRight:MAXimum



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.Emask.MfRight.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: