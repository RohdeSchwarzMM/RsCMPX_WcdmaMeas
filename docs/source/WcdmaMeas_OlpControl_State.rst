State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:OLPControl:STATe

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:OLPControl:STATe



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.OlpControl.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.olpControl.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_OlpControl_State_All.rst