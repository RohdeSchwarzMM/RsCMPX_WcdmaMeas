Minimum
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:LIST:CDPower:DPCCh:MINimum

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:LIST:CDPower:DPCCh:MINimum



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.CdPower.Dpcch.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: