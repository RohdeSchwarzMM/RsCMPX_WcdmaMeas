M<Minus>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Ch1 .. Ch2
	rc = driver.wcdmaMeas.multiEval.listPy.spectrum.aclr.m.repcap_minus_get()
	driver.wcdmaMeas.multiEval.listPy.spectrum.aclr.m.repcap_minus_set(repcap.Minus.Ch1)





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.Spectrum.Aclr.M.MCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.listPy.spectrum.aclr.m.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_ListPy_Spectrum_Aclr_M_Average.rst
	WcdmaMeas_MultiEval_ListPy_Spectrum_Aclr_M_Current.rst
	WcdmaMeas_MultiEval_ListPy_Spectrum_Aclr_M_Maximum.rst