CdPower
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIST:SEGMent<nr>:CDPower

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIST:SEGMent<nr>:CDPower



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.ListPy.Segment.CdPower.CdPowerCls
	:members:
	:undoc-members:
	:noindex: