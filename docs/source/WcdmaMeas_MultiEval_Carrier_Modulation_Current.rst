Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:MODulation:CURRent
	single: READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:MODulation:CURRent
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:MODulation:CURRent

.. code-block:: python

	CALCulate:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:MODulation:CURRent
	READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:MODulation:CURRent
	FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:MODulation:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Modulation.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: