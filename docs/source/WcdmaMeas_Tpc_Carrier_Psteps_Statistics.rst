Statistics
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:PSTeps:STATistics
	single: FETCh:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:PSTeps:STATistics

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:PSTeps:STATistics
	FETCh:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:PSTeps:STATistics



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Tpc.Carrier.Psteps.Statistics.StatisticsCls
	:members:
	:undoc-members:
	:noindex: