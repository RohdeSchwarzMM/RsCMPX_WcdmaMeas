Catalog
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:WCDMa:MEASurement<instance>:OOSYnc:CATalog:SOURce

.. code-block:: python

	TRIGger:WCDMa:MEASurement<instance>:OOSYnc:CATalog:SOURce



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Trigger.WcdmaMeas.OoSync.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: