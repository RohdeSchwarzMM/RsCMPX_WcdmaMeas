Maximum
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:LIST:SPECtrum:ACLR:M<nr>:MAXimum

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:LIST:SPECtrum:ACLR:M<nr>:MAXimum



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.Spectrum.Aclr.M.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: