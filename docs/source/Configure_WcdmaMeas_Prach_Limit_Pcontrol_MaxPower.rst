MaxPower
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:PRACh:LIMit:PCONtrol:MAXPower:URPClass
	single: CONFigure:WCDMa:MEASurement<instance>:PRACh:LIMit:PCONtrol:MAXPower
	single: CONFigure:WCDMa:MEASurement<instance>:PRACh:LIMit:PCONtrol:MAXPower:ACTive

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:PRACh:LIMit:PCONtrol:MAXPower:URPClass
	CONFigure:WCDMa:MEASurement<instance>:PRACh:LIMit:PCONtrol:MAXPower
	CONFigure:WCDMa:MEASurement<instance>:PRACh:LIMit:PCONtrol:MAXPower:ACTive



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Prach.Limit.Pcontrol.MaxPower.MaxPowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wcdmaMeas.prach.limit.pcontrol.maxPower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WcdmaMeas_Prach_Limit_Pcontrol_MaxPower_UserDefined.rst