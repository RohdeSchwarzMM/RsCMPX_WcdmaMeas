Perror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:PRACh:LIMit:PERRor

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:PRACh:LIMit:PERRor



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Prach.Limit.Perror.PerrorCls
	:members:
	:undoc-members:
	:noindex: