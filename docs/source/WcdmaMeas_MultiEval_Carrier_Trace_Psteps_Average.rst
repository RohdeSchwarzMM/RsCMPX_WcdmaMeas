Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:PSTeps:AVERage
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:PSTeps:AVERage

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:PSTeps:AVERage
	FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:PSTeps:AVERage



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.Psteps.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: