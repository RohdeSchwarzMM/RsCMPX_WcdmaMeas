Carrier<Carrier>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.wcdmaMeas.tpc.carrier.repcap_carrier_get()
	driver.wcdmaMeas.tpc.carrier.repcap_carrier_set(repcap.Carrier.Nr1)





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Tpc.Carrier.CarrierCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.tpc.carrier.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_Tpc_Carrier_Psteps.rst
	WcdmaMeas_Tpc_Carrier_Trace.rst
	WcdmaMeas_Tpc_Carrier_UePower.rst