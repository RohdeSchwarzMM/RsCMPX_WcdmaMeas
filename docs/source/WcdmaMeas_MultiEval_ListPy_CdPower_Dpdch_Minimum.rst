Minimum
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:LIST:CDPower:DPDCh:MINimum

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:LIST:CDPower:DPDCh:MINimum



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.CdPower.Dpdch.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: