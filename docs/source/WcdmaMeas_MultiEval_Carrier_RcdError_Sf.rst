Sf
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:RCDerror:SF
	single: READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:RCDerror:SF

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:RCDerror:SF
	READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:RCDerror:SF



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.RcdError.Sf.SfCls
	:members:
	:undoc-members:
	:noindex: