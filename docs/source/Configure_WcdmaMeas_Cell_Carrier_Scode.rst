Scode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:CELL:CARRier<carrier>:SCODe

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:CELL:CARRier<carrier>:SCODe



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Cell.Carrier.Scode.ScodeCls
	:members:
	:undoc-members:
	:noindex: