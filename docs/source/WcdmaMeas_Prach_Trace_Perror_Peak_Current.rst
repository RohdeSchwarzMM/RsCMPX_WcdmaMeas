Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:PRACh:TRACe:PERRor:PEAK:CURRent
	single: READ:WCDMa:MEASurement<instance>:PRACh:TRACe:PERRor:PEAK:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:PRACh:TRACe:PERRor:PEAK:CURRent
	READ:WCDMa:MEASurement<instance>:PRACh:TRACe:PERRor:PEAK:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Prach.Trace.Perror.Peak.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: