PhsDpcch
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIMit:PHSDpcch

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIMit:PHSDpcch



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.Limit.PhsDpcch.PhsDpcchCls
	:members:
	:undoc-members:
	:noindex: