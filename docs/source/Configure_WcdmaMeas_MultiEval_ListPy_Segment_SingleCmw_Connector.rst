Connector
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIST:SEGMent<nr>:CMWS:CONNector

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIST:SEGMent<nr>:CMWS:CONNector



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.ListPy.Segment.SingleCmw.Connector.ConnectorCls
	:members:
	:undoc-members:
	:noindex: