Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:WCDMa:MEASurement<instance>:MEValuation:SPECtrum:MAXimum
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:SPECtrum:MAXimum
	single: READ:WCDMa:MEASurement<instance>:MEValuation:SPECtrum:MAXimum

.. code-block:: python

	CALCulate:WCDMa:MEASurement<instance>:MEValuation:SPECtrum:MAXimum
	FETCh:WCDMa:MEASurement<instance>:MEValuation:SPECtrum:MAXimum
	READ:WCDMa:MEASurement<instance>:MEValuation:SPECtrum:MAXimum



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Spectrum.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: