State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:OOSYnc:STATe

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:OOSYnc:STATe



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.OoSync.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.ooSync.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_OoSync_State_All.rst