State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:TPC:STATe

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:TPC:STATe



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Tpc.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.tpc.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_Tpc_State_All.rst