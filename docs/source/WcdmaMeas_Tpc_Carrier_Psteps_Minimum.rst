Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:PSTeps:MINimum
	single: FETCh:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:PSTeps:MINimum
	single: CALCulate:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:PSTeps:MINimum

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:PSTeps:MINimum
	FETCh:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:PSTeps:MINimum
	CALCulate:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:PSTeps:MINimum



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Tpc.Carrier.Psteps.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: