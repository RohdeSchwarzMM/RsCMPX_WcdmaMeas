UeChannels
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:UECHannels:BSFSelection

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:UECHannels:BSFSelection



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.UeChannels.UeChannelsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wcdmaMeas.ueChannels.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WcdmaMeas_UeChannels_Carrier.rst