Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:PHD:CURRent
	single: READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:PHD:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:PHD:CURRent
	READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:PHD:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.Phd.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: