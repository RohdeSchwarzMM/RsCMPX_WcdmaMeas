Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:MERRor:CHIP:AVERage
	single: READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:MERRor:CHIP:AVERage

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:MERRor:CHIP:AVERage
	READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:MERRor:CHIP:AVERage



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.Merror.Chip.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: