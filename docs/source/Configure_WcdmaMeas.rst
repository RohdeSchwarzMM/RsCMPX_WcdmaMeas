WcdmaMeas
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.WcdmaMeasCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wcdmaMeas.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WcdmaMeas_Carrier.rst
	Configure_WcdmaMeas_Cell.rst
	Configure_WcdmaMeas_MultiEval.rst
	Configure_WcdmaMeas_OlpControl.rst
	Configure_WcdmaMeas_OoSync.rst
	Configure_WcdmaMeas_Prach.rst
	Configure_WcdmaMeas_RfSettings.rst
	Configure_WcdmaMeas_Tpc.rst
	Configure_WcdmaMeas_UeChannels.rst
	Configure_WcdmaMeas_UeSignal.rst