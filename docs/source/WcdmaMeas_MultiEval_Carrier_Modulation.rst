Modulation
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.carrier.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_Carrier_Modulation_Average.rst
	WcdmaMeas_MultiEval_Carrier_Modulation_Current.rst
	WcdmaMeas_MultiEval_Carrier_Modulation_Maximum.rst
	WcdmaMeas_MultiEval_Carrier_Modulation_StandardDev.rst