CdPower
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.CdPower.CdPowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.carrier.trace.cdPower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_Carrier_Trace_CdPower_Dpcch.rst
	WcdmaMeas_MultiEval_Carrier_Trace_CdPower_Dpdch.rst
	WcdmaMeas_MultiEval_Carrier_Trace_CdPower_Edpcch.rst
	WcdmaMeas_MultiEval_Carrier_Trace_CdPower_Edpdch.rst
	WcdmaMeas_MultiEval_Carrier_Trace_CdPower_Hsdpcch.rst