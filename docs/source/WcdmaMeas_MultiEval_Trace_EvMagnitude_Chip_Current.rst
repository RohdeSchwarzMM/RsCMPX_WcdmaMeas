Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EVMagnitude:CHIP:CURRent
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EVMagnitude:CHIP:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EVMagnitude:CHIP:CURRent
	FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EVMagnitude:CHIP:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.EvMagnitude.Chip.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: