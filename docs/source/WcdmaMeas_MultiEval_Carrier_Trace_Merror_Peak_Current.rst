Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:MERRor:PEAK:CURRent
	single: READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:MERRor:PEAK:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:MERRor:PEAK:CURRent
	READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:MERRor:PEAK:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.Merror.Peak.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: