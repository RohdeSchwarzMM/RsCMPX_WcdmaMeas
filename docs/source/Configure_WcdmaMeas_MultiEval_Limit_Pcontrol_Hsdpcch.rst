Hsdpcch
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIMit:PCONtrol:HSDPcch

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIMit:PCONtrol:HSDPcch



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.Limit.Pcontrol.Hsdpcch.HsdpcchCls
	:members:
	:undoc-members:
	:noindex: