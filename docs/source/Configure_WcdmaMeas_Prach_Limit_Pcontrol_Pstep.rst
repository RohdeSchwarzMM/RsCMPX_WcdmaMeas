Pstep
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:PRACh:LIMit:PCONtrol:PSTep

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:PRACh:LIMit:PCONtrol:PSTep



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Prach.Limit.Pcontrol.Pstep.PstepCls
	:members:
	:undoc-members:
	:noindex: