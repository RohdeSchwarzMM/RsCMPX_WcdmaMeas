Segment<Segment>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr200
	rc = driver.wcdmaMeas.multiEval.listPy.segment.repcap_segment_get()
	driver.wcdmaMeas.multiEval.listPy.segment.repcap_segment_set(repcap.Segment.Nr1)





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.Segment.SegmentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.listPy.segment.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_ListPy_Segment_CdError.rst
	WcdmaMeas_MultiEval_ListPy_Segment_CdPower.rst
	WcdmaMeas_MultiEval_ListPy_Segment_Modulation.rst
	WcdmaMeas_MultiEval_ListPy_Segment_Pcde.rst
	WcdmaMeas_MultiEval_ListPy_Segment_Phd.rst
	WcdmaMeas_MultiEval_ListPy_Segment_Spectrum.rst
	WcdmaMeas_MultiEval_ListPy_Segment_UePower.rst