StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:RCDerror:SDEViation
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:RCDerror:SDEViation
	single: READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:RCDerror:SDEViation

.. code-block:: python

	CALCulate:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:RCDerror:SDEViation
	FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:RCDerror:SDEViation
	READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:RCDerror:SDEViation



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.RcdError.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: