Current
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:LIST:SPECtrum:EMASk:BC:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:LIST:SPECtrum:EMASk:BC:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.Spectrum.Emask.Bc.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: