RcdError
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.RcdError.RcdErrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.carrier.rcdError.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_Carrier_RcdError_Average.rst
	WcdmaMeas_MultiEval_Carrier_RcdError_Current.rst
	WcdmaMeas_MultiEval_Carrier_RcdError_Maximum.rst
	WcdmaMeas_MultiEval_Carrier_RcdError_OcInfo.rst
	WcdmaMeas_MultiEval_Carrier_RcdError_Sf.rst
	WcdmaMeas_MultiEval_Carrier_RcdError_StandardDev.rst