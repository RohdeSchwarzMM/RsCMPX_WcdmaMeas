Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:PCDE:MAXimum
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:PCDE:MAXimum

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:PCDE:MAXimum
	FETCh:WCDMa:MEASurement<instance>:MEValuation:PCDE:MAXimum



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Pcde.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: