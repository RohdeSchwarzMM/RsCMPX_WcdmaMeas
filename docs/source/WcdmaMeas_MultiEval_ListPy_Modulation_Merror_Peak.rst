Peak
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.Modulation.Merror.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.listPy.modulation.merror.peak.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_ListPy_Modulation_Merror_Peak_Average.rst
	WcdmaMeas_MultiEval_ListPy_Modulation_Merror_Peak_Current.rst
	WcdmaMeas_MultiEval_ListPy_Modulation_Merror_Peak_Maximum.rst
	WcdmaMeas_MultiEval_ListPy_Modulation_Merror_Peak_StandardDev.rst