Ctfc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:TPC:LIMit:CTFC

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:TPC:LIMit:CTFC



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Tpc.Limit.Ctfc.CtfcCls
	:members:
	:undoc-members:
	:noindex: