PhDhsDpcch
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:WCDMa:MEASurement<instance>:MEValuation:MODulation:PHDHsdpcch
	single: READ:WCDMa:MEASurement<instance>:MEValuation:MODulation:PHDHsdpcch
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:MODulation:PHDHsdpcch

.. code-block:: python

	CALCulate:WCDMa:MEASurement<instance>:MEValuation:MODulation:PHDHsdpcch
	READ:WCDMa:MEASurement<instance>:MEValuation:MODulation:PHDHsdpcch
	FETCh:WCDMa:MEASurement<instance>:MEValuation:MODulation:PHDHsdpcch



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Modulation.PhDhsDpcch.PhDhsDpcchCls
	:members:
	:undoc-members:
	:noindex: