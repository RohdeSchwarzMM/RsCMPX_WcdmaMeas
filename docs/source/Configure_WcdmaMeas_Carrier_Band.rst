Band
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:CARRier<carrier>:BAND

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:CARRier<carrier>:BAND



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Carrier.Band.BandCls
	:members:
	:undoc-members:
	:noindex: