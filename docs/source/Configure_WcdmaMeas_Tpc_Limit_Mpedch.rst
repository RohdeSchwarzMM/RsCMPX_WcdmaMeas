Mpedch
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:TPC:LIMit:MPEDch

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:TPC:LIMit:MPEDch



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Tpc.Limit.Mpedch.MpedchCls
	:members:
	:undoc-members:
	:noindex: