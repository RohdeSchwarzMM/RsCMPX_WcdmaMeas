MultiEval
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: STOP:WCDMa:MEASurement<instance>:MEValuation
	single: ABORt:WCDMa:MEASurement<instance>:MEValuation
	single: INITiate:WCDMa:MEASurement<instance>:MEValuation

.. code-block:: python

	STOP:WCDMa:MEASurement<instance>:MEValuation
	ABORt:WCDMa:MEASurement<instance>:MEValuation
	INITiate:WCDMa:MEASurement<instance>:MEValuation



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_Ber.rst
	WcdmaMeas_MultiEval_Carrier.rst
	WcdmaMeas_MultiEval_ListPy.rst
	WcdmaMeas_MultiEval_Modulation.rst
	WcdmaMeas_MultiEval_Pcde.rst
	WcdmaMeas_MultiEval_Spectrum.rst
	WcdmaMeas_MultiEval_State.rst
	WcdmaMeas_MultiEval_Trace.rst