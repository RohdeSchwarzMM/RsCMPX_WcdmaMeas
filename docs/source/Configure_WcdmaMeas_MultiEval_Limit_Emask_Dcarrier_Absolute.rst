Absolute
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIMit:EMASk:DCARrier:ABSolute

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIMit:EMASk:DCARrier:ABSolute



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.Limit.Emask.Dcarrier.Absolute.AbsoluteCls
	:members:
	:undoc-members:
	:noindex: