OoSync
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:WCDMa:MEASurement<instance>:OOSYnc
	single: READ:WCDMa:MEASurement<instance>:OOSYnc
	single: FETCh:WCDMa:MEASurement<instance>:OOSYnc
	single: STOP:WCDMa:MEASurement<instance>:OOSYnc
	single: ABORt:WCDMa:MEASurement<instance>:OOSYnc
	single: INITiate:WCDMa:MEASurement<instance>:OOSYnc

.. code-block:: python

	CALCulate:WCDMa:MEASurement<instance>:OOSYnc
	READ:WCDMa:MEASurement<instance>:OOSYnc
	FETCh:WCDMa:MEASurement<instance>:OOSYnc
	STOP:WCDMa:MEASurement<instance>:OOSYnc
	ABORt:WCDMa:MEASurement<instance>:OOSYnc
	INITiate:WCDMa:MEASurement<instance>:OOSYnc



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.OoSync.OoSyncCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.ooSync.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_OoSync_State.rst