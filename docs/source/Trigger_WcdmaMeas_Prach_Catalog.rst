Catalog
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:WCDMa:MEASurement<instance>:PRACh:CATalog:SOURce

.. code-block:: python

	TRIGger:WCDMa:MEASurement<instance>:PRACh:CATalog:SOURce



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Trigger.WcdmaMeas.Prach.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: