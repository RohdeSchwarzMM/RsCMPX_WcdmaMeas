UserDefined
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:PRACh:LIMit:PCONtrol:MAXPower:UDEFined

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:PRACh:LIMit:PCONtrol:MAXPower:UDEFined



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Prach.Limit.Pcontrol.MaxPower.UserDefined.UserDefinedCls
	:members:
	:undoc-members:
	:noindex: