Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:UEPower:AVERage
	single: READ:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:UEPower:AVERage
	single: CALCulate:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:UEPower:AVERage

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:UEPower:AVERage
	READ:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:UEPower:AVERage
	CALCulate:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:UEPower:AVERage



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Tpc.Carrier.UePower.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: