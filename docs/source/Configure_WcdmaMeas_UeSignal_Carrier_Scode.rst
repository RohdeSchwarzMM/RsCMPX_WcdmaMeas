Scode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:UESignal:CARRier<carrier>:SCODe

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:UESignal:CARRier<carrier>:SCODe



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.UeSignal.Carrier.Scode.ScodeCls
	:members:
	:undoc-members:
	:noindex: