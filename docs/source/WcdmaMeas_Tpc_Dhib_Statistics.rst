Statistics
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:TPC:DHIB:STATistics
	single: FETCh:WCDMa:MEASurement<instance>:TPC:DHIB:STATistics

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:TPC:DHIB:STATistics
	FETCh:WCDMa:MEASurement<instance>:TPC:DHIB:STATistics



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Tpc.Dhib.Statistics.StatisticsCls
	:members:
	:undoc-members:
	:noindex: