Spectrum
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.Spectrum.SpectrumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.listPy.spectrum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_ListPy_Spectrum_Aclr.rst
	WcdmaMeas_MultiEval_ListPy_Spectrum_Average.rst
	WcdmaMeas_MultiEval_ListPy_Spectrum_Cpower.rst
	WcdmaMeas_MultiEval_ListPy_Spectrum_Current.rst
	WcdmaMeas_MultiEval_ListPy_Spectrum_Emask.rst
	WcdmaMeas_MultiEval_ListPy_Spectrum_Maximum.rst
	WcdmaMeas_MultiEval_ListPy_Spectrum_Obw.rst
	WcdmaMeas_MultiEval_ListPy_Spectrum_UePower.rst