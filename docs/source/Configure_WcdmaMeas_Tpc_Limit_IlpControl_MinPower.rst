MinPower
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:TPC:LIMit:ILPControl:MINPower

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:TPC:LIMit:ILPControl:MINPower



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Tpc.Limit.IlpControl.MinPower.MinPowerCls
	:members:
	:undoc-members:
	:noindex: