Tpc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:WCDMa:MEASurement<instance>:TPC:DELay
	single: TRIGger:WCDMa:MEASurement<instance>:TPC:MGAP
	single: TRIGger:WCDMa:MEASurement<instance>:TPC:THReshold
	single: TRIGger:WCDMa:MEASurement<instance>:TPC:SLOPe
	single: TRIGger:WCDMa:MEASurement<instance>:TPC:TOUT
	single: TRIGger:WCDMa:MEASurement<instance>:TPC:SOURce

.. code-block:: python

	TRIGger:WCDMa:MEASurement<instance>:TPC:DELay
	TRIGger:WCDMa:MEASurement<instance>:TPC:MGAP
	TRIGger:WCDMa:MEASurement<instance>:TPC:THReshold
	TRIGger:WCDMa:MEASurement<instance>:TPC:SLOPe
	TRIGger:WCDMa:MEASurement<instance>:TPC:TOUT
	TRIGger:WCDMa:MEASurement<instance>:TPC:SOURce



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Trigger.WcdmaMeas.Tpc.TpcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.wcdmaMeas.tpc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_WcdmaMeas_Tpc_Catalog.rst