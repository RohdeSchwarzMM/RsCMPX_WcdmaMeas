Pcde
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.Pcde.PcdeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.listPy.pcde.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_ListPy_Pcde_Code.rst
	WcdmaMeas_MultiEval_ListPy_Pcde_Current.rst
	WcdmaMeas_MultiEval_ListPy_Pcde_Error.rst
	WcdmaMeas_MultiEval_ListPy_Pcde_Maximum.rst
	WcdmaMeas_MultiEval_ListPy_Pcde_Phase.rst