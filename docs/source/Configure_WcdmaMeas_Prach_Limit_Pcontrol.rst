Pcontrol
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:PRACh:LIMit:PCONtrol:OFFPower

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:PRACh:LIMit:PCONtrol:OFFPower



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Prach.Limit.Pcontrol.PcontrolCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wcdmaMeas.prach.limit.pcontrol.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WcdmaMeas_Prach_Limit_Pcontrol_MaxPower.rst
	Configure_WcdmaMeas_Prach_Limit_Pcontrol_OlPower.rst
	Configure_WcdmaMeas_Prach_Limit_Pcontrol_Pstep.rst