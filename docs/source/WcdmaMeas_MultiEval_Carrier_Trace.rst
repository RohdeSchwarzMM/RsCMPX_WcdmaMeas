Trace
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.carrier.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_Carrier_Trace_CdError.rst
	WcdmaMeas_MultiEval_Carrier_Trace_CdPower.rst
	WcdmaMeas_MultiEval_Carrier_Trace_EvMagnitude.rst
	WcdmaMeas_MultiEval_Carrier_Trace_FreqError.rst
	WcdmaMeas_MultiEval_Carrier_Trace_Merror.rst
	WcdmaMeas_MultiEval_Carrier_Trace_Perror.rst
	WcdmaMeas_MultiEval_Carrier_Trace_Psteps.rst
	WcdmaMeas_MultiEval_Carrier_Trace_RcdError.rst
	WcdmaMeas_MultiEval_Carrier_Trace_UePower.rst