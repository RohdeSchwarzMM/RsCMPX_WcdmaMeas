Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:TPC:DHIB:MINimum
	single: FETCh:WCDMa:MEASurement<instance>:TPC:DHIB:MINimum

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:TPC:DHIB:MINimum
	FETCh:WCDMa:MEASurement<instance>:TPC:DHIB:MINimum



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Tpc.Dhib.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: