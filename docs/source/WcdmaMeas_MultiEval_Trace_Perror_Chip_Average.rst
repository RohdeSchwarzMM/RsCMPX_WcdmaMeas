Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:PERRor:CHIP:AVERage
	single: READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:PERRor:CHIP:AVERage

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:PERRor:CHIP:AVERage
	READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:PERRor:CHIP:AVERage



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.Perror.Chip.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: