Result
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:TXM
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:RCDerror
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:IQ
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:BER
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:PSTeps
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:PHD
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:FERRor
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:UEPower
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult[:ALL]
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:CDERror
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:CDPower
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:CDPMonitor
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:EMASk
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:ACLR
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:PERRor
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:EVMagnitude
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:MERRor

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:TXM
	CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:RCDerror
	CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:IQ
	CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:BER
	CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:PSTeps
	CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:PHD
	CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:FERRor
	CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:UEPower
	CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult[:ALL]
	CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:CDERror
	CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:CDPower
	CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:CDPMonitor
	CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:EMASk
	CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:ACLR
	CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:PERRor
	CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:EVMagnitude
	CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:MERRor



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wcdmaMeas.multiEval.result.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WcdmaMeas_MultiEval_Result_Chip.rst