IlpControl
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:TPC:ILPControl:MLENgth
	single: CONFigure:WCDMa:MEASurement<instance>:TPC:ILPControl:TSSegment
	single: CONFigure:WCDMa:MEASurement<instance>:TPC:ILPControl:AEXecution

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:TPC:ILPControl:MLENgth
	CONFigure:WCDMa:MEASurement<instance>:TPC:ILPControl:TSSegment
	CONFigure:WCDMa:MEASurement<instance>:TPC:ILPControl:AEXecution



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Tpc.IlpControl.IlpControlCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wcdmaMeas.tpc.ilpControl.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WcdmaMeas_Tpc_IlpControl_Tsef.rst
	Configure_WcdmaMeas_Tpc_IlpControl_Tsgh.rst