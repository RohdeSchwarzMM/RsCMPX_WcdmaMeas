Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:RFSettings:CARRier<carrier>:FREQuency

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:RFSettings:CARRier<carrier>:FREQuency



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.RfSettings.Carrier.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: