Had
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.Spectrum.Emask.Had.HadCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.listPy.spectrum.emask.had.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_ListPy_Spectrum_Emask_Had_Average.rst
	WcdmaMeas_MultiEval_ListPy_Spectrum_Emask_Had_Current.rst
	WcdmaMeas_MultiEval_ListPy_Spectrum_Emask_Had_Maximum.rst