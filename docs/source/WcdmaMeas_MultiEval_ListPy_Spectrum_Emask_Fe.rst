Fe
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.Spectrum.Emask.Fe.FeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.listPy.spectrum.emask.fe.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_ListPy_Spectrum_Emask_Fe_Average.rst
	WcdmaMeas_MultiEval_ListPy_Spectrum_Emask_Fe_Current.rst
	WcdmaMeas_MultiEval_ListPy_Spectrum_Emask_Fe_Maximum.rst