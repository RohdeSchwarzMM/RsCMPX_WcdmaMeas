Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:PERRor:PEAK:MAXimum
	single: READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:PERRor:PEAK:MAXimum

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:PERRor:PEAK:MAXimum
	READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:PERRor:PEAK:MAXimum



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.Perror.Peak.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: