Limit
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIMit:IQOFfset
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIMit:IQIMbalance
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIMit:CFERror

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIMit:IQOFfset
	CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIMit:IQIMbalance
	CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIMit:CFERror



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.Limit.LimitCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wcdmaMeas.multiEval.limit.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WcdmaMeas_MultiEval_Limit_Aclr.rst
	Configure_WcdmaMeas_MultiEval_Limit_Emask.rst
	Configure_WcdmaMeas_MultiEval_Limit_EvMagnitude.rst
	Configure_WcdmaMeas_MultiEval_Limit_Merror.rst
	Configure_WcdmaMeas_MultiEval_Limit_Pcontrol.rst
	Configure_WcdmaMeas_MultiEval_Limit_Perror.rst
	Configure_WcdmaMeas_MultiEval_Limit_Phd.rst
	Configure_WcdmaMeas_MultiEval_Limit_PhsDpcch.rst
	Configure_WcdmaMeas_MultiEval_Limit_RcdError.rst