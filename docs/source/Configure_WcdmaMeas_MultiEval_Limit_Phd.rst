Phd
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIMit:PHD

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIMit:PHD



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.Limit.Phd.PhdCls
	:members:
	:undoc-members:
	:noindex: