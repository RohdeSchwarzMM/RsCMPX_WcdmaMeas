OcInfo
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:RCDerror:OCINfo
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:RCDerror:OCINfo

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:RCDerror:OCINfo
	FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:RCDerror:OCINfo



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.RcdError.OcInfo.OcInfoCls
	:members:
	:undoc-members:
	:noindex: