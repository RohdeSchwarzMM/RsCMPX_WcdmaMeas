Hsdpcch
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.CdPower.Hsdpcch.HsdpcchCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.carrier.trace.cdPower.hsdpcch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_Carrier_Trace_CdPower_Hsdpcch_Average.rst
	WcdmaMeas_MultiEval_Carrier_Trace_CdPower_Hsdpcch_Current.rst
	WcdmaMeas_MultiEval_Carrier_Trace_CdPower_Hsdpcch_Maximum.rst
	WcdmaMeas_MultiEval_Carrier_Trace_CdPower_Hsdpcch_Minimum.rst
	WcdmaMeas_MultiEval_Carrier_Trace_CdPower_Hsdpcch_StandardDev.rst