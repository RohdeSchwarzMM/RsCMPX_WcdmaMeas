Maximum
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:LIST:CDERror:EDPDch<nr>:MAXimum

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:LIST:CDERror:EDPDch<nr>:MAXimum



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.CdError.Edpdch.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: