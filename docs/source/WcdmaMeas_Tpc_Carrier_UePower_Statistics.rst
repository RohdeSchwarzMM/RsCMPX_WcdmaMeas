Statistics
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:UEPower:STATistics
	single: READ:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:UEPower:STATistics

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:UEPower:STATistics
	READ:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:UEPower:STATistics



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Tpc.Carrier.UePower.Statistics.StatisticsCls
	:members:
	:undoc-members:
	:noindex: