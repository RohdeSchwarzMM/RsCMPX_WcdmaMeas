Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:TPC:TOTal:UEPower:MINimum
	single: READ:WCDMa:MEASurement<instance>:TPC:TOTal:UEPower:MINimum

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:TPC:TOTal:UEPower:MINimum
	READ:WCDMa:MEASurement<instance>:TPC:TOTal:UEPower:MINimum



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Tpc.Total.UePower.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: