Spectrum
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.Segment.Spectrum.SpectrumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.listPy.segment.spectrum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_ListPy_Segment_Spectrum_Average.rst
	WcdmaMeas_MultiEval_ListPy_Segment_Spectrum_Current.rst
	WcdmaMeas_MultiEval_ListPy_Segment_Spectrum_Maximum.rst