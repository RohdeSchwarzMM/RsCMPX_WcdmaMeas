StandardDev
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:LIST:MODulation:PERRor:PEAK:SDEViation

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:LIST:MODulation:PERRor:PEAK:SDEViation



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.Modulation.Perror.Peak.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: