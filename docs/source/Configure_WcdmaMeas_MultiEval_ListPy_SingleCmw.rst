SingleCmw
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIST:CMWS:CMODe

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIST:CMWS:CMODe



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.ListPy.SingleCmw.SingleCmwCls
	:members:
	:undoc-members:
	:noindex: