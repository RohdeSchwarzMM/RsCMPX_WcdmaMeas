Carrier<Carrier>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.configure.wcdmaMeas.ueChannels.carrier.repcap_carrier_get()
	driver.configure.wcdmaMeas.ueChannels.carrier.repcap_carrier_set(repcap.Carrier.Nr1)





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.UeChannels.Carrier.CarrierCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wcdmaMeas.ueChannels.carrier.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WcdmaMeas_UeChannels_Carrier_Dpcch.rst
	Configure_WcdmaMeas_UeChannels_Carrier_Dpdch.rst
	Configure_WcdmaMeas_UeChannels_Carrier_Edpcch.rst
	Configure_WcdmaMeas_UeChannels_Carrier_Edpdch.rst
	Configure_WcdmaMeas_UeChannels_Carrier_Hsdpcch.rst