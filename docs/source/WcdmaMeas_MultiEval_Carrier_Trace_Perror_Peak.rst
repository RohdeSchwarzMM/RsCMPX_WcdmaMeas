Peak
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.Perror.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.carrier.trace.perror.peak.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_Carrier_Trace_Perror_Peak_Average.rst
	WcdmaMeas_MultiEval_Carrier_Trace_Perror_Peak_Current.rst
	WcdmaMeas_MultiEval_Carrier_Trace_Perror_Peak_Maximum.rst
	WcdmaMeas_MultiEval_Carrier_Trace_Perror_Peak_StandardDev.rst