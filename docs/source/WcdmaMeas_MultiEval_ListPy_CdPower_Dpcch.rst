Dpcch
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.CdPower.Dpcch.DpcchCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.listPy.cdPower.dpcch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_ListPy_CdPower_Dpcch_Average.rst
	WcdmaMeas_MultiEval_ListPy_CdPower_Dpcch_Current.rst
	WcdmaMeas_MultiEval_ListPy_CdPower_Dpcch_Maximum.rst
	WcdmaMeas_MultiEval_ListPy_CdPower_Dpcch_Minimum.rst
	WcdmaMeas_MultiEval_ListPy_CdPower_Dpcch_StandardDev.rst