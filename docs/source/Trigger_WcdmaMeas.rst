WcdmaMeas
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Trigger.WcdmaMeas.WcdmaMeasCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.wcdmaMeas.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_WcdmaMeas_MultiEval.rst
	Trigger_WcdmaMeas_OlpControl.rst
	Trigger_WcdmaMeas_OoSync.rst
	Trigger_WcdmaMeas_Prach.rst
	Trigger_WcdmaMeas_Tpc.rst