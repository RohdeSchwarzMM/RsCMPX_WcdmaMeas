Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:EVMagnitude:PEAK:CURRent
	single: READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:EVMagnitude:PEAK:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:EVMagnitude:PEAK:CURRent
	READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:EVMagnitude:PEAK:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.EvMagnitude.Peak.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: