Current
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:LIST:SPECtrum:ACLR:M<nr>:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:LIST:SPECtrum:ACLR:M<nr>:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.Spectrum.Aclr.M.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: