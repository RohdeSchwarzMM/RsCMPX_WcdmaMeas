Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:MERRor[:RMS]:MAXimum
	single: READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:MERRor[:RMS]:MAXimum

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:MERRor[:RMS]:MAXimum
	READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:MERRor[:RMS]:MAXimum



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.Merror.Rms.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: