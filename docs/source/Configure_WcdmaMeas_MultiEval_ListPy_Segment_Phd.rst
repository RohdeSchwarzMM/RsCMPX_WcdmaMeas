Phd
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIST:SEGMent<nr>:PHD

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIST:SEGMent<nr>:PHD



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.ListPy.Segment.Phd.PhdCls
	:members:
	:undoc-members:
	:noindex: