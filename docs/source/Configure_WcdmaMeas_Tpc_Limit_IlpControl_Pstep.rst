Pstep
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:TPC:LIMit:ILPControl:PSTep

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:TPC:LIMit:ILPControl:PSTep



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Tpc.Limit.IlpControl.Pstep.PstepCls
	:members:
	:undoc-members:
	:noindex: