Merror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:PRACh:LIMit:MERRor

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:PRACh:LIMit:MERRor



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Prach.Limit.Merror.MerrorCls
	:members:
	:undoc-members:
	:noindex: