StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:PERRor:PEAK:SDEViation
	single: READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:PERRor:PEAK:SDEViation

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:PERRor:PEAK:SDEViation
	READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:PERRor:PEAK:SDEViation



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.Perror.Peak.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: