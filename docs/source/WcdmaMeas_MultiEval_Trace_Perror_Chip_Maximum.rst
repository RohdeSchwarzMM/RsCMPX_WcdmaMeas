Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:PERRor:CHIP:MAXimum
	single: READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:PERRor:CHIP:MAXimum

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:PERRor:CHIP:MAXimum
	READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:PERRor:CHIP:MAXimum



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.Perror.Chip.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: