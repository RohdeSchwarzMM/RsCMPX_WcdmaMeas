Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:CDPower:EDPCch:MAXimum
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:CDPower:EDPCch:MAXimum

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:CDPower:EDPCch:MAXimum
	FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:CDPower:EDPCch:MAXimum



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.CdPower.Edpcch.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: