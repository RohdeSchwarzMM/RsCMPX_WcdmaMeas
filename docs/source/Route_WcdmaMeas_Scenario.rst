Scenario
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ROUTe:WCDMa:MEASurement<instance>:SCENario:CSPath
	single: ROUTe:WCDMa:MEASurement<instance>:SCENario

.. code-block:: python

	ROUTe:WCDMa:MEASurement<instance>:SCENario:CSPath
	ROUTe:WCDMa:MEASurement<instance>:SCENario



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Route.WcdmaMeas.Scenario.ScenarioCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.wcdmaMeas.scenario.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_WcdmaMeas_Scenario_MaProtocol.rst
	Route_WcdmaMeas_Scenario_Salone.rst