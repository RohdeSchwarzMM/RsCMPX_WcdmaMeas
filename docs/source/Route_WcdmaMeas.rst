WcdmaMeas
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:WCDMa:MEASurement<instance>

.. code-block:: python

	ROUTe:WCDMa:MEASurement<instance>



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Route.WcdmaMeas.WcdmaMeasCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.wcdmaMeas.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_WcdmaMeas_Scenario.rst