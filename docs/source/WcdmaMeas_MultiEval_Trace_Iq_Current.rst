Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:IQ:CURRent
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:IQ:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:IQ:CURRent
	FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:IQ:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.Iq.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: