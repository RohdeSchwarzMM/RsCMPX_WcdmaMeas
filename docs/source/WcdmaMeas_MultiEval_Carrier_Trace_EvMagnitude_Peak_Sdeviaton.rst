Sdeviaton
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:EVMagnitude:PEAK:SDEViaton

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:EVMagnitude:PEAK:SDEViaton



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.EvMagnitude.Peak.Sdeviaton.SdeviatonCls
	:members:
	:undoc-members:
	:noindex: