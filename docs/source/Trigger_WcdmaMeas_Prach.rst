Prach
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:WCDMa:MEASurement<instance>:PRACh:DELay
	single: TRIGger:WCDMa:MEASurement<instance>:PRACh:MGAP
	single: TRIGger:WCDMa:MEASurement<instance>:PRACh:THReshold
	single: TRIGger:WCDMa:MEASurement<instance>:PRACh:SLOPe
	single: TRIGger:WCDMa:MEASurement<instance>:PRACh:TOUT
	single: TRIGger:WCDMa:MEASurement<instance>:PRACh:SOURce

.. code-block:: python

	TRIGger:WCDMa:MEASurement<instance>:PRACh:DELay
	TRIGger:WCDMa:MEASurement<instance>:PRACh:MGAP
	TRIGger:WCDMa:MEASurement<instance>:PRACh:THReshold
	TRIGger:WCDMa:MEASurement<instance>:PRACh:SLOPe
	TRIGger:WCDMa:MEASurement<instance>:PRACh:TOUT
	TRIGger:WCDMa:MEASurement<instance>:PRACh:SOURce



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Trigger.WcdmaMeas.Prach.PrachCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.wcdmaMeas.prach.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_WcdmaMeas_Prach_Catalog.rst