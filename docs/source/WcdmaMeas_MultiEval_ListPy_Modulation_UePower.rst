UePower
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.Modulation.UePower.UePowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.listPy.modulation.uePower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_ListPy_Modulation_UePower_Average.rst
	WcdmaMeas_MultiEval_ListPy_Modulation_UePower_Current.rst
	WcdmaMeas_MultiEval_ListPy_Modulation_UePower_Maximum.rst
	WcdmaMeas_MultiEval_ListPy_Modulation_UePower_StandardDev.rst