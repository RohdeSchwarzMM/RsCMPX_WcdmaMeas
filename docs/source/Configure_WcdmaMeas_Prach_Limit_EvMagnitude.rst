EvMagnitude
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:PRACh:LIMit:EVMagnitude

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:PRACh:LIMit:EVMagnitude



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Prach.Limit.EvMagnitude.EvMagnitudeCls
	:members:
	:undoc-members:
	:noindex: