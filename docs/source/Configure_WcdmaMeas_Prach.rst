Prach
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:PRACh:TOUT
	single: CONFigure:WCDMa:MEASurement<instance>:PRACh:MPReamble
	single: CONFigure:WCDMa:MEASurement<instance>:PRACh:PPReamble
	single: CONFigure:WCDMa:MEASurement<instance>:PRACh:OFFPower
	single: CONFigure:WCDMa:MEASurement<instance>:PRACh:MOEXception

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:PRACh:TOUT
	CONFigure:WCDMa:MEASurement<instance>:PRACh:MPReamble
	CONFigure:WCDMa:MEASurement<instance>:PRACh:PPReamble
	CONFigure:WCDMa:MEASurement<instance>:PRACh:OFFPower
	CONFigure:WCDMa:MEASurement<instance>:PRACh:MOEXception



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Prach.PrachCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wcdmaMeas.prach.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WcdmaMeas_Prach_Limit.rst
	Configure_WcdmaMeas_Prach_Result.rst