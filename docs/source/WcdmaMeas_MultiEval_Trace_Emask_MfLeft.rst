MfLeft
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.Emask.MfLeft.MfLeftCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.trace.emask.mfLeft.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_Trace_Emask_MfLeft_Average.rst
	WcdmaMeas_MultiEval_Trace_Emask_MfLeft_Current.rst
	WcdmaMeas_MultiEval_Trace_Emask_MfLeft_Maximum.rst