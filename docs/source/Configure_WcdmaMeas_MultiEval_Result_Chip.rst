Chip
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:CHIP:PERRor
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:CHIP:MERRor
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:CHIP:EVM

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:CHIP:PERRor
	CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:CHIP:MERRor
	CONFigure:WCDMa:MEASurement<instance>:MEValuation:RESult:CHIP:EVM



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.Result.Chip.ChipCls
	:members:
	:undoc-members:
	:noindex: