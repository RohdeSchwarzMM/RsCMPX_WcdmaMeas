CdError
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.Segment.CdError.CdErrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.listPy.segment.cdError.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_ListPy_Segment_CdError_Average.rst
	WcdmaMeas_MultiEval_ListPy_Segment_CdError_Current.rst
	WcdmaMeas_MultiEval_ListPy_Segment_CdError_Maximum.rst
	WcdmaMeas_MultiEval_ListPy_Segment_CdError_StandardDev.rst