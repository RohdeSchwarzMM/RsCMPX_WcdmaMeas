Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:CDERror:CURRent
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:CDERror:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:CDERror:CURRent
	FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:CDERror:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.CdError.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: