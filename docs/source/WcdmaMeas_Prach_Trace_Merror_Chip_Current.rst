Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:PRACh:TRACe:MERRor:CHIP:CURRent
	single: READ:WCDMa:MEASurement<instance>:PRACh:TRACe:MERRor:CHIP:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:PRACh:TRACe:MERRor:CHIP:CURRent
	READ:WCDMa:MEASurement<instance>:PRACh:TRACe:MERRor:CHIP:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Prach.Trace.Merror.Chip.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: