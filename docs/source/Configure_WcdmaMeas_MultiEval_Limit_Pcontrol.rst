Pcontrol
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.Limit.Pcontrol.PcontrolCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wcdmaMeas.multiEval.limit.pcontrol.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WcdmaMeas_MultiEval_Limit_Pcontrol_EpStep.rst
	Configure_WcdmaMeas_MultiEval_Limit_Pcontrol_Hsdpcch.rst