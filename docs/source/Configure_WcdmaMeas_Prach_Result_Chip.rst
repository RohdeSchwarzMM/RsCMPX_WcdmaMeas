Chip
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:PRACh:RESult:CHIP:UEPower
	single: CONFigure:WCDMa:MEASurement<instance>:PRACh:RESult:CHIP:PERRor
	single: CONFigure:WCDMa:MEASurement<instance>:PRACh:RESult:CHIP:MERRor
	single: CONFigure:WCDMa:MEASurement<instance>:PRACh:RESult:CHIP:EVM

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:PRACh:RESult:CHIP:UEPower
	CONFigure:WCDMa:MEASurement<instance>:PRACh:RESult:CHIP:PERRor
	CONFigure:WCDMa:MEASurement<instance>:PRACh:RESult:CHIP:MERRor
	CONFigure:WCDMa:MEASurement<instance>:PRACh:RESult:CHIP:EVM



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Prach.Result.Chip.ChipCls
	:members:
	:undoc-members:
	:noindex: