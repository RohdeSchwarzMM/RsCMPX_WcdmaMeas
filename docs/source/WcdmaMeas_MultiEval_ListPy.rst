ListPy
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.ListPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.listPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_ListPy_CdError.rst
	WcdmaMeas_MultiEval_ListPy_CdPower.rst
	WcdmaMeas_MultiEval_ListPy_Modulation.rst
	WcdmaMeas_MultiEval_ListPy_Pcde.rst
	WcdmaMeas_MultiEval_ListPy_Phd.rst
	WcdmaMeas_MultiEval_ListPy_Segment.rst
	WcdmaMeas_MultiEval_ListPy_Spectrum.rst
	WcdmaMeas_MultiEval_ListPy_Sreliability.rst
	WcdmaMeas_MultiEval_ListPy_UePower.rst