StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:CDERror:DPCCh:SDEViation
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:CDERror:DPCCh:SDEViation

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:CDERror:DPCCh:SDEViation
	FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:CDERror:DPCCh:SDEViation



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.CdError.Dpcch.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: