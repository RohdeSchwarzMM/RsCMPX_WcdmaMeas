Emask
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.Emask.EmaskCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.trace.emask.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_Trace_Emask_HkfLeft.rst
	WcdmaMeas_MultiEval_Trace_Emask_HkfRight.rst
	WcdmaMeas_MultiEval_Trace_Emask_Kfilter.rst
	WcdmaMeas_MultiEval_Trace_Emask_MfLeft.rst
	WcdmaMeas_MultiEval_Trace_Emask_MfRight.rst