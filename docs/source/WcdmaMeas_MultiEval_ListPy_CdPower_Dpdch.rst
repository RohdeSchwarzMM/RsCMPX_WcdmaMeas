Dpdch
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.CdPower.Dpdch.DpdchCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.listPy.cdPower.dpdch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_ListPy_CdPower_Dpdch_Average.rst
	WcdmaMeas_MultiEval_ListPy_CdPower_Dpdch_Current.rst
	WcdmaMeas_MultiEval_ListPy_CdPower_Dpdch_Maximum.rst
	WcdmaMeas_MultiEval_ListPy_CdPower_Dpdch_Minimum.rst
	WcdmaMeas_MultiEval_ListPy_CdPower_Dpdch_StandardDev.rst