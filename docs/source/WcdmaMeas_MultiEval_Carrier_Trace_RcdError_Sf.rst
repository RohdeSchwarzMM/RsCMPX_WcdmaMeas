Sf
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.RcdError.Sf.SfCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.carrier.trace.rcdError.sf.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_Carrier_Trace_RcdError_Sf_Dpcch.rst
	WcdmaMeas_MultiEval_Carrier_Trace_RcdError_Sf_Dpdch.rst
	WcdmaMeas_MultiEval_Carrier_Trace_RcdError_Sf_Edpcch.rst
	WcdmaMeas_MultiEval_Carrier_Trace_RcdError_Sf_Edpdch.rst
	WcdmaMeas_MultiEval_Carrier_Trace_RcdError_Sf_Hsdpcch.rst