Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:HKFLeft:MAXimum
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:HKFLeft:MAXimum

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:HKFLeft:MAXimum
	FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:HKFLeft:MAXimum



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.Emask.HkfLeft.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: