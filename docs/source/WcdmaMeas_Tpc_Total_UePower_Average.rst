Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:TPC:TOTal:UEPower:AVERage
	single: READ:WCDMa:MEASurement<instance>:TPC:TOTal:UEPower:AVERage

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:TPC:TOTal:UEPower:AVERage
	READ:WCDMa:MEASurement<instance>:TPC:TOTal:UEPower:AVERage



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Tpc.Total.UePower.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: