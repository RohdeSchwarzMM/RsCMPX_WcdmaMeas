Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:UEPower:MAXimum
	single: READ:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:UEPower:MAXimum
	single: CALCulate:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:UEPower:MAXimum

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:UEPower:MAXimum
	READ:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:UEPower:MAXimum
	CALCulate:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:UEPower:MAXimum



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Tpc.Carrier.UePower.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: