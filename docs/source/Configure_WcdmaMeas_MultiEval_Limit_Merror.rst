Merror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIMit:MERRor

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIMit:MERRor



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.Limit.Merror.MerrorCls
	:members:
	:undoc-members:
	:noindex: