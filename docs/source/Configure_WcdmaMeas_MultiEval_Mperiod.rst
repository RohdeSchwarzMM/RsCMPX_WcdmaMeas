Mperiod
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:MPERiod:MODulation

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:MEValuation:MPERiod:MODulation



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.Mperiod.MperiodCls
	:members:
	:undoc-members:
	:noindex: