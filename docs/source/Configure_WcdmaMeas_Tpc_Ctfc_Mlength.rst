Mlength
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:TPC:CTFC:MLENgth

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:TPC:CTFC:MLENgth



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Tpc.Ctfc.Mlength.MlengthCls
	:members:
	:undoc-members:
	:noindex: