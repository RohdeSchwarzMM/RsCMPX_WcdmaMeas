Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:CDPower:MINimum
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:CDPower:MINimum

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:CDPower:MINimum
	FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:CDPower:MINimum



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.CdPower.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: