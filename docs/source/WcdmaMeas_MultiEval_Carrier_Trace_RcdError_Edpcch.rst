Edpcch
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.RcdError.Edpcch.EdpcchCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.carrier.trace.rcdError.edpcch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_Carrier_Trace_RcdError_Edpcch_Average.rst
	WcdmaMeas_MultiEval_Carrier_Trace_RcdError_Edpcch_Current.rst
	WcdmaMeas_MultiEval_Carrier_Trace_RcdError_Edpcch_Maximum.rst
	WcdmaMeas_MultiEval_Carrier_Trace_RcdError_Edpcch_StandardDev.rst