Dpdch
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIMit:RCDerror:EECDp:CARRier<carrier>:DPDCh

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIMit:RCDerror:EECDp:CARRier<carrier>:DPDCh



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.Limit.RcdError.Eecdp.Carrier.Dpdch.DpdchCls
	:members:
	:undoc-members:
	:noindex: