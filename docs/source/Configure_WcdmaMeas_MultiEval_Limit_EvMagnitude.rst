EvMagnitude
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIMit:EVMagnitude

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIMit:EVMagnitude



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.Limit.EvMagnitude.EvMagnitudeCls
	:members:
	:undoc-members:
	:noindex: