RsCMPX_WcdmaMeas API Structure
========================================


.. rubric:: Global RepCaps

.. code-block:: python
	
	driver = RsCMPX_WcdmaMeas('TCPIP::192.168.2.101::hislip0')
	# Instance range: Inst1 .. Inst32
	rc = driver.repcap_instance_get()
	driver.repcap_instance_set(repcap.Instance.Inst1)

.. autoclass:: RsCMPX_WcdmaMeas.RsCMPX_WcdmaMeas
	:members:
	:undoc-members:
	:noindex:

.. rubric:: Subgroups

.. toctree::
	:maxdepth: 6
	:glob:

	Configure.rst
	Route.rst
	Trigger.rst
	WcdmaMeas.rst