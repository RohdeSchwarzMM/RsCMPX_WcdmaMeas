Scount
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:SCOunt:BER
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:SCOunt:MODulation
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:SCOunt:SPECtrum

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:MEValuation:SCOunt:BER
	CONFigure:WCDMa:MEASurement<instance>:MEValuation:SCOunt:MODulation
	CONFigure:WCDMa:MEASurement<instance>:MEValuation:SCOunt:SPECtrum



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.Scount.ScountCls
	:members:
	:undoc-members:
	:noindex: