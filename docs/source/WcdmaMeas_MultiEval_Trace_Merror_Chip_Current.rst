Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:MERRor:CHIP:CURRent
	single: READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:MERRor:CHIP:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:MERRor:CHIP:CURRent
	READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:MERRor:CHIP:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.Merror.Chip.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: