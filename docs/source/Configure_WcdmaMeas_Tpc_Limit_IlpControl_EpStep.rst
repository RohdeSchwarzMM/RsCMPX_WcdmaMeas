EpStep
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:TPC:LIMit:ILPControl:EPSTep

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:TPC:LIMit:ILPControl:EPSTep



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Tpc.Limit.IlpControl.EpStep.EpStepCls
	:members:
	:undoc-members:
	:noindex: