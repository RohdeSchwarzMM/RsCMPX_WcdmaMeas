Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:MODulation:AVERage
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:MODulation:AVERage
	single: READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:MODulation:AVERage

.. code-block:: python

	CALCulate:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:MODulation:AVERage
	FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:MODulation:AVERage
	READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:MODulation:AVERage



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Modulation.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: