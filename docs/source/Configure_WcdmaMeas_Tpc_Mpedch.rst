Mpedch
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:TPC:MPEDch:MLENgth
	single: CONFigure:WCDMa:MEASurement<instance>:TPC:MPEDch:AEXecution

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:TPC:MPEDch:MLENgth
	CONFigure:WCDMa:MEASurement<instance>:TPC:MPEDch:AEXecution



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Tpc.Mpedch.MpedchCls
	:members:
	:undoc-members:
	:noindex: