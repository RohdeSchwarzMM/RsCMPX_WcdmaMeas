Current
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:LIST:CDERror:EDPDch<nr>:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:LIST:CDERror:EDPDch<nr>:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.CdError.Edpdch.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: