Current
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:LIST:UEPower:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:LIST:UEPower:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.UePower.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: