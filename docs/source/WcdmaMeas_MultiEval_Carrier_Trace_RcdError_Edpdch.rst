Edpdch<EdpdChannel>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.wcdmaMeas.multiEval.carrier.trace.rcdError.edpdch.repcap_edpdChannel_get()
	driver.wcdmaMeas.multiEval.carrier.trace.rcdError.edpdch.repcap_edpdChannel_set(repcap.EdpdChannel.Nr1)





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.RcdError.Edpdch.EdpdchCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.carrier.trace.rcdError.edpdch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_Carrier_Trace_RcdError_Edpdch_Average.rst
	WcdmaMeas_MultiEval_Carrier_Trace_RcdError_Edpdch_Current.rst
	WcdmaMeas_MultiEval_Carrier_Trace_RcdError_Edpdch_Maximum.rst
	WcdmaMeas_MultiEval_Carrier_Trace_RcdError_Edpdch_StandardDev.rst