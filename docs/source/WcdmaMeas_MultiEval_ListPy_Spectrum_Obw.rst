Obw
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.Spectrum.Obw.ObwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.listPy.spectrum.obw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_ListPy_Spectrum_Obw_Average.rst
	WcdmaMeas_MultiEval_ListPy_Spectrum_Obw_Current.rst
	WcdmaMeas_MultiEval_ListPy_Spectrum_Obw_Maximum.rst