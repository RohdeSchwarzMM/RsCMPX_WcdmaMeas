Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:MERRor:PEAK:MAXimum
	single: READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:MERRor:PEAK:MAXimum

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:MERRor:PEAK:MAXimum
	READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:MERRor:PEAK:MAXimum



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.Merror.Peak.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: