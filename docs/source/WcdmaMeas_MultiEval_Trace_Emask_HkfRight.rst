HkfRight
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.Emask.HkfRight.HkfRightCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.trace.emask.hkfRight.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_Trace_Emask_HkfRight_Average.rst
	WcdmaMeas_MultiEval_Trace_Emask_HkfRight_Current.rst
	WcdmaMeas_MultiEval_Trace_Emask_HkfRight_Maximum.rst