Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:CDPower:AVERage
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:CDPower:AVERage

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:CDPower:AVERage
	FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:CDPower:AVERage



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.CdPower.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: