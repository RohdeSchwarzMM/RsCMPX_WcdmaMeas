UserDefined
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:TPC:LIMit:ILPControl:MAXPower:UDEFined

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:TPC:LIMit:ILPControl:MAXPower:UDEFined



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Tpc.Limit.IlpControl.MaxPower.UserDefined.UserDefinedCls
	:members:
	:undoc-members:
	:noindex: