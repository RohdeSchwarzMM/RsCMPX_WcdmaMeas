Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:PERRor:PEAK:AVERage
	single: READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:PERRor:PEAK:AVERage

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:PERRor:PEAK:AVERage
	READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:PERRor:PEAK:AVERage



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.Perror.Peak.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: