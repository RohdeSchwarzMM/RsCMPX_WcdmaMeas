Dpcch
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:RCDerror:SF:DPCCh
	single: READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:RCDerror:SF:DPCCh

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:RCDerror:SF:DPCCh
	READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:RCDerror:SF:DPCCh



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.RcdError.Sf.Dpcch.DpcchCls
	:members:
	:undoc-members:
	:noindex: