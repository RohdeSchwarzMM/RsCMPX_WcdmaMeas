P<Plus>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Ch1 .. Ch2
	rc = driver.wcdmaMeas.multiEval.listPy.spectrum.aclr.p.repcap_plus_get()
	driver.wcdmaMeas.multiEval.listPy.spectrum.aclr.p.repcap_plus_set(repcap.Plus.Ch1)





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.Spectrum.Aclr.P.PCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.listPy.spectrum.aclr.p.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_ListPy_Spectrum_Aclr_P_Average.rst
	WcdmaMeas_MultiEval_ListPy_Spectrum_Aclr_P_Current.rst
	WcdmaMeas_MultiEval_ListPy_Spectrum_Aclr_P_Maximum.rst