Limit
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:TPC:LIMit:DHIB

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:TPC:LIMit:DHIB



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Tpc.Limit.LimitCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wcdmaMeas.tpc.limit.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WcdmaMeas_Tpc_Limit_Ctfc.rst
	Configure_WcdmaMeas_Tpc_Limit_IlpControl.rst
	Configure_WcdmaMeas_Tpc_Limit_Mpedch.rst
	Configure_WcdmaMeas_Tpc_Limit_Ulcm.rst