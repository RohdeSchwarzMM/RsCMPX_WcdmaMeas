Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:HKFRight:CURRent
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:HKFRight:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:HKFRight:CURRent
	FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:HKFRight:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.Emask.HkfRight.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: