Kfilter
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.Emask.Kfilter.KfilterCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.trace.emask.kfilter.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_Trace_Emask_Kfilter_Average.rst
	WcdmaMeas_MultiEval_Trace_Emask_Kfilter_Current.rst
	WcdmaMeas_MultiEval_Trace_Emask_Kfilter_Maximum.rst