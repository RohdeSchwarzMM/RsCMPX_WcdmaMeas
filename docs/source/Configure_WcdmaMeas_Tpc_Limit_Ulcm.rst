Ulcm
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Tpc.Limit.Ulcm.UlcmCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wcdmaMeas.tpc.limit.ulcm.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WcdmaMeas_Tpc_Limit_Ulcm_Pa.rst
	Configure_WcdmaMeas_Tpc_Limit_Ulcm_Pb.rst