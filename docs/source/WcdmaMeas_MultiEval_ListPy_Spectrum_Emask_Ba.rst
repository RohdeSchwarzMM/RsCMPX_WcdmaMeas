Ba
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.Spectrum.Emask.Ba.BaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.listPy.spectrum.emask.ba.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_ListPy_Spectrum_Emask_Ba_Average.rst
	WcdmaMeas_MultiEval_ListPy_Spectrum_Emask_Ba_Current.rst
	WcdmaMeas_MultiEval_ListPy_Spectrum_Emask_Ba_Maximum.rst