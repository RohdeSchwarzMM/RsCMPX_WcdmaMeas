Sscalar
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:SSCalar:MODulation

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:MEValuation:SSCalar:MODulation



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.Sscalar.SscalarCls
	:members:
	:undoc-members:
	:noindex: