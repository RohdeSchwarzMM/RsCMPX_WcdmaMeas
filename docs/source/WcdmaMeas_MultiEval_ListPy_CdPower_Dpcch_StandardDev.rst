StandardDev
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:LIST:CDPower:DPCCh:SDEViation

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:LIST:CDPower:DPCCh:SDEViation



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.CdPower.Dpcch.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: