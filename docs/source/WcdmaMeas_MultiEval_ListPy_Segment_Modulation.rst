Modulation
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.Segment.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.listPy.segment.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_ListPy_Segment_Modulation_Average.rst
	WcdmaMeas_MultiEval_ListPy_Segment_Modulation_Current.rst
	WcdmaMeas_MultiEval_ListPy_Segment_Modulation_Maximum.rst
	WcdmaMeas_MultiEval_ListPy_Segment_Modulation_StandardDev.rst