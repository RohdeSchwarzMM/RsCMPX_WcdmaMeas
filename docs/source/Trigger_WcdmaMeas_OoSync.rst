OoSync
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:WCDMa:MEASurement<instance>:OOSYnc:DELay
	single: TRIGger:WCDMa:MEASurement<instance>:OOSYnc:MGAP
	single: TRIGger:WCDMa:MEASurement<instance>:OOSYnc:THReshold
	single: TRIGger:WCDMa:MEASurement<instance>:OOSYnc:SLOPe
	single: TRIGger:WCDMa:MEASurement<instance>:OOSYnc:TOUT
	single: TRIGger:WCDMa:MEASurement<instance>:OOSYnc:SOURce

.. code-block:: python

	TRIGger:WCDMa:MEASurement<instance>:OOSYnc:DELay
	TRIGger:WCDMa:MEASurement<instance>:OOSYnc:MGAP
	TRIGger:WCDMa:MEASurement<instance>:OOSYnc:THReshold
	TRIGger:WCDMa:MEASurement<instance>:OOSYnc:SLOPe
	TRIGger:WCDMa:MEASurement<instance>:OOSYnc:TOUT
	TRIGger:WCDMa:MEASurement<instance>:OOSYnc:SOURce



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Trigger.WcdmaMeas.OoSync.OoSyncCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.wcdmaMeas.ooSync.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_WcdmaMeas_OoSync_Catalog.rst