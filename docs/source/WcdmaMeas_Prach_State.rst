State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:PRACh:STATe

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:PRACh:STATe



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Prach.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.prach.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_Prach_State_All.rst