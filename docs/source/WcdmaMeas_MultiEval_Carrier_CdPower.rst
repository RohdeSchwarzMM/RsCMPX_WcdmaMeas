CdPower
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.CdPower.CdPowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.carrier.cdPower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_Carrier_CdPower_Average.rst
	WcdmaMeas_MultiEval_Carrier_CdPower_Current.rst
	WcdmaMeas_MultiEval_Carrier_CdPower_Maximum.rst
	WcdmaMeas_MultiEval_Carrier_CdPower_Minimum.rst
	WcdmaMeas_MultiEval_Carrier_CdPower_StandardDev.rst