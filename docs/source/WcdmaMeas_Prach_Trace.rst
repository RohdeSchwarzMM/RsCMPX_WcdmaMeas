Trace
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Prach.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.prach.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_Prach_Trace_EvMagnitude.rst
	WcdmaMeas_Prach_Trace_FreqError.rst
	WcdmaMeas_Prach_Trace_Iq.rst
	WcdmaMeas_Prach_Trace_Merror.rst
	WcdmaMeas_Prach_Trace_Perror.rst
	WcdmaMeas_Prach_Trace_Psteps.rst
	WcdmaMeas_Prach_Trace_UePower.rst