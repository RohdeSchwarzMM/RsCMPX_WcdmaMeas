Dpdch
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.CdError.Dpdch.DpdchCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.listPy.cdError.dpdch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_ListPy_CdError_Dpdch_Average.rst
	WcdmaMeas_MultiEval_ListPy_CdError_Dpdch_Current.rst
	WcdmaMeas_MultiEval_ListPy_CdError_Dpdch_Maximum.rst
	WcdmaMeas_MultiEval_ListPy_CdError_Dpdch_StandardDev.rst