Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:MODulation:MAXimum
	single: READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:MODulation:MAXimum
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:MODulation:MAXimum

.. code-block:: python

	CALCulate:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:MODulation:MAXimum
	READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:MODulation:MAXimum
	FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:MODulation:MAXimum



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Modulation.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: