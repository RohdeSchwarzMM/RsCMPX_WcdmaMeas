Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:PRACh:TRACe:PERRor:CHIP:CURRent
	single: READ:WCDMa:MEASurement<instance>:PRACh:TRACe:PERRor:CHIP:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:PRACh:TRACe:PERRor:CHIP:CURRent
	READ:WCDMa:MEASurement<instance>:PRACh:TRACe:PERRor:CHIP:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Prach.Trace.Perror.Chip.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: