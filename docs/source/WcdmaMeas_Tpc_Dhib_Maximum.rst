Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:TPC:DHIB:MAXimum
	single: FETCh:WCDMa:MEASurement<instance>:TPC:DHIB:MAXimum
	single: CALCulate:WCDMa:MEASurement<instance>:TPC:DHIB:MAXimum

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:TPC:DHIB:MAXimum
	FETCh:WCDMa:MEASurement<instance>:TPC:DHIB:MAXimum
	CALCulate:WCDMa:MEASurement<instance>:TPC:DHIB:MAXimum



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Tpc.Dhib.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: