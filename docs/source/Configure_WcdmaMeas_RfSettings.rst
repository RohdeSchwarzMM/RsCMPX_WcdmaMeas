RfSettings
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:RFSettings:EATTenuation
	single: CONFigure:WCDMa:MEASurement<instance>:RFSettings:UMARgin
	single: CONFigure:WCDMa:MEASurement<instance>:RFSettings:ENPower

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:RFSettings:EATTenuation
	CONFigure:WCDMa:MEASurement<instance>:RFSettings:UMARgin
	CONFigure:WCDMa:MEASurement<instance>:RFSettings:ENPower



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.RfSettings.RfSettingsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wcdmaMeas.rfSettings.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WcdmaMeas_RfSettings_Carrier.rst
	Configure_WcdmaMeas_RfSettings_Dcarrier.rst