Dmode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:DMODe:MODulation

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:MEValuation:DMODe:MODulation



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.Dmode.DmodeCls
	:members:
	:undoc-members:
	:noindex: