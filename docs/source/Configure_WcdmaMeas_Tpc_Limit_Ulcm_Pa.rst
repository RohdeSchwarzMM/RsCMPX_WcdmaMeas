Pa
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:TPC:LIMit:ULCM:PA

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:TPC:LIMit:ULCM:PA



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Tpc.Limit.Ulcm.Pa.PaCls
	:members:
	:undoc-members:
	:noindex: