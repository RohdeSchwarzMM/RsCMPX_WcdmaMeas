Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:UEPower:MINimum
	single: READ:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:UEPower:MINimum
	single: CALCulate:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:UEPower:MINimum

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:UEPower:MINimum
	READ:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:UEPower:MINimum
	CALCulate:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:UEPower:MINimum



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Tpc.Carrier.UePower.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: