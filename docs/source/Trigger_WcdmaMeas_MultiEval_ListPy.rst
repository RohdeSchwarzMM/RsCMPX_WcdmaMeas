ListPy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:WCDMa:MEASurement<instance>:MEValuation:LIST:MODE

.. code-block:: python

	TRIGger:WCDMa:MEASurement<instance>:MEValuation:LIST:MODE



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Trigger.WcdmaMeas.MultiEval.ListPy.ListPyCls
	:members:
	:undoc-members:
	:noindex: