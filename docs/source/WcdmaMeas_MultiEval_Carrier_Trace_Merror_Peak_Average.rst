Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:MERRor:PEAK:AVERage
	single: READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:MERRor:PEAK:AVERage

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:MERRor:PEAK:AVERage
	READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:MERRor:PEAK:AVERage



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.Merror.Peak.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: