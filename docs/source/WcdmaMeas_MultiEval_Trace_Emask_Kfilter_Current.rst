Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:KFILter:CURRent
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:KFILter:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:KFILter:CURRent
	FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:KFILter:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.Emask.Kfilter.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: