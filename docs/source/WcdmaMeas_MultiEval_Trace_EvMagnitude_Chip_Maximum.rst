Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EVMagnitude:CHIP:MAXimum
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EVMagnitude:CHIP:MAXimum

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EVMagnitude:CHIP:MAXimum
	FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EVMagnitude:CHIP:MAXimum



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.EvMagnitude.Chip.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: