Maximum
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:LIST:MODulation:TTERror:MAXimum

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:LIST:MODulation:TTERror:MAXimum



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.Modulation.TtError.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: