UeSignal
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:UESignal:DPDCh
	single: CONFigure:WCDMa:MEASurement<instance>:UESignal:ULConfig
	single: CONFigure:WCDMa:MEASurement<instance>:UESignal:SFORmat
	single: CONFigure:WCDMa:MEASurement<instance>:UESignal:CMPattern

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:UESignal:DPDCh
	CONFigure:WCDMa:MEASurement<instance>:UESignal:ULConfig
	CONFigure:WCDMa:MEASurement<instance>:UESignal:SFORmat
	CONFigure:WCDMa:MEASurement<instance>:UESignal:CMPattern



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.UeSignal.UeSignalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wcdmaMeas.ueSignal.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WcdmaMeas_UeSignal_Carrier.rst