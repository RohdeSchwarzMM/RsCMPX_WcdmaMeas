Average
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:LIST:CDERror:HSDPcch:AVERage

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:LIST:CDERror:HSDPcch:AVERage



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.CdError.Hsdpcch.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: