Prach
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: STOP:WCDMa:MEASurement<instance>:PRACh
	single: ABORt:WCDMa:MEASurement<instance>:PRACh
	single: INITiate:WCDMa:MEASurement<instance>:PRACh

.. code-block:: python

	STOP:WCDMa:MEASurement<instance>:PRACh
	ABORt:WCDMa:MEASurement<instance>:PRACh
	INITiate:WCDMa:MEASurement<instance>:PRACh



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Prach.PrachCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.prach.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_Prach_OffPower.rst
	WcdmaMeas_Prach_Preamble.rst
	WcdmaMeas_Prach_State.rst
	WcdmaMeas_Prach_Trace.rst