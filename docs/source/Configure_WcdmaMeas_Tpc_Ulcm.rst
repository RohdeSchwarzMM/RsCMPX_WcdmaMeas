Ulcm
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:TPC:ULCM:MLENgth
	single: CONFigure:WCDMa:MEASurement<instance>:TPC:ULCM:AEXecution

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:TPC:ULCM:MLENgth
	CONFigure:WCDMa:MEASurement<instance>:TPC:ULCM:AEXecution



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Tpc.Ulcm.UlcmCls
	:members:
	:undoc-members:
	:noindex: