Statistics
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:TPC:TOTal:UEPower:STATistics
	single: READ:WCDMa:MEASurement<instance>:TPC:TOTal:UEPower:STATistics

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:TPC:TOTal:UEPower:STATistics
	READ:WCDMa:MEASurement<instance>:TPC:TOTal:UEPower:STATistics



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Tpc.Total.UePower.Statistics.StatisticsCls
	:members:
	:undoc-members:
	:noindex: