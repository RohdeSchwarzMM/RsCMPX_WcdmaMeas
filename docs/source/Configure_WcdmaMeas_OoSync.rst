OoSync
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:OOSYnc:AADPchlevel
	single: CONFigure:WCDMa:MEASurement<instance>:OOSYnc:TOUT

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:OOSYnc:AADPchlevel
	CONFigure:WCDMa:MEASurement<instance>:OOSYnc:TOUT



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.OoSync.OoSyncCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wcdmaMeas.ooSync.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WcdmaMeas_OoSync_Limit.rst