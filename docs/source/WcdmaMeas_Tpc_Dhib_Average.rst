Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:TPC:DHIB:AVERage
	single: FETCh:WCDMa:MEASurement<instance>:TPC:DHIB:AVERage
	single: CALCulate:WCDMa:MEASurement<instance>:TPC:DHIB:AVERage

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:TPC:DHIB:AVERage
	FETCh:WCDMa:MEASurement<instance>:TPC:DHIB:AVERage
	CALCulate:WCDMa:MEASurement<instance>:TPC:DHIB:AVERage



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Tpc.Dhib.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: