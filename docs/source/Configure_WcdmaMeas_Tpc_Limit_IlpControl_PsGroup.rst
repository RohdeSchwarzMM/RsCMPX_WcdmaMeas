PsGroup
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:TPC:LIMit:ILPControl:PSGRoup

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:TPC:LIMit:ILPControl:PSGRoup



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Tpc.Limit.IlpControl.PsGroup.PsGroupCls
	:members:
	:undoc-members:
	:noindex: