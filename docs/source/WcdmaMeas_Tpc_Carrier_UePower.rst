UePower
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Tpc.Carrier.UePower.UePowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.tpc.carrier.uePower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_Tpc_Carrier_UePower_Average.rst
	WcdmaMeas_Tpc_Carrier_UePower_Maximum.rst
	WcdmaMeas_Tpc_Carrier_UePower_Minimum.rst
	WcdmaMeas_Tpc_Carrier_UePower_Statistics.rst