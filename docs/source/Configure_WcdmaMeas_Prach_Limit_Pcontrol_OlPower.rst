OlPower
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:PRACh:LIMit:PCONtrol:OLPower

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:PRACh:LIMit:PCONtrol:OLPower



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Prach.Limit.Pcontrol.OlPower.OlPowerCls
	:members:
	:undoc-members:
	:noindex: