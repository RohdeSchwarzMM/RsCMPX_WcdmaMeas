Aclr
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIMit:ACLR:ABSolute

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIMit:ACLR:ABSolute



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.Limit.Aclr.AclrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wcdmaMeas.multiEval.limit.aclr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WcdmaMeas_MultiEval_Limit_Aclr_Relative.rst