Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:EVMagnitude[:RMS]:MAXimum
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:EVMagnitude[:RMS]:MAXimum

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:EVMagnitude[:RMS]:MAXimum
	FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:EVMagnitude[:RMS]:MAXimum



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.EvMagnitude.Rms.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: