Catalog
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:WCDMa:MEASurement<instance>:TPC:CATalog:SOURce

.. code-block:: python

	TRIGger:WCDMa:MEASurement<instance>:TPC:CATalog:SOURce



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Trigger.WcdmaMeas.Tpc.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: