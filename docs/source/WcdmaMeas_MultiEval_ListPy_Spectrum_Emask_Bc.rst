Bc
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.Spectrum.Emask.Bc.BcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.listPy.spectrum.emask.bc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_ListPy_Spectrum_Emask_Bc_Average.rst
	WcdmaMeas_MultiEval_ListPy_Spectrum_Emask_Bc_Current.rst
	WcdmaMeas_MultiEval_ListPy_Spectrum_Emask_Bc_Maximum.rst