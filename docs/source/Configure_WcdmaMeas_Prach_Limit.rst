Limit
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:PRACh:LIMit:IQOFfset
	single: CONFigure:WCDMa:MEASurement<instance>:PRACh:LIMit:IQIMbalance
	single: CONFigure:WCDMa:MEASurement<instance>:PRACh:LIMit:CFERror

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:PRACh:LIMit:IQOFfset
	CONFigure:WCDMa:MEASurement<instance>:PRACh:LIMit:IQIMbalance
	CONFigure:WCDMa:MEASurement<instance>:PRACh:LIMit:CFERror



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Prach.Limit.LimitCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wcdmaMeas.prach.limit.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WcdmaMeas_Prach_Limit_EvMagnitude.rst
	Configure_WcdmaMeas_Prach_Limit_Merror.rst
	Configure_WcdmaMeas_Prach_Limit_Pcontrol.rst
	Configure_WcdmaMeas_Prach_Limit_Perror.rst