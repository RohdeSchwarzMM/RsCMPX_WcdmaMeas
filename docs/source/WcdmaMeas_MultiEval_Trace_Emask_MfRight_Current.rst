Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:MFRight:CURRent
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:MFRight:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:MFRight:CURRent
	FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:MFRight:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.Emask.MfRight.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: