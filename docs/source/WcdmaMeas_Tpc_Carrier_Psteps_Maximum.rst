Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:PSTeps:MAXimum
	single: FETCh:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:PSTeps:MAXimum
	single: CALCulate:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:PSTeps:MAXimum

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:PSTeps:MAXimum
	FETCh:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:PSTeps:MAXimum
	CALCulate:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:PSTeps:MAXimum



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Tpc.Carrier.Psteps.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: