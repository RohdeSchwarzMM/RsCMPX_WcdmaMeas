Ber
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:BER
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:BER

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:BER
	FETCh:WCDMa:MEASurement<instance>:MEValuation:BER



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Ber.BerCls
	:members:
	:undoc-members:
	:noindex: