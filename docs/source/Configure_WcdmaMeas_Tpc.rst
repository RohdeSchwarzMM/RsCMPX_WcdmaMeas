Tpc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:TPC:CSELection
	single: CONFigure:WCDMa:MEASurement<instance>:TPC:SETup
	single: CONFigure:WCDMa:MEASurement<instance>:TPC:MODE
	single: CONFigure:WCDMa:MEASurement<instance>:TPC:MOEXception
	single: CONFigure:WCDMa:MEASurement<instance>:TPC:TOUT

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:TPC:CSELection
	CONFigure:WCDMa:MEASurement<instance>:TPC:SETup
	CONFigure:WCDMa:MEASurement<instance>:TPC:MODE
	CONFigure:WCDMa:MEASurement<instance>:TPC:MOEXception
	CONFigure:WCDMa:MEASurement<instance>:TPC:TOUT



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Tpc.TpcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wcdmaMeas.tpc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WcdmaMeas_Tpc_Ctfc.rst
	Configure_WcdmaMeas_Tpc_Dhib.rst
	Configure_WcdmaMeas_Tpc_IlpControl.rst
	Configure_WcdmaMeas_Tpc_Limit.rst
	Configure_WcdmaMeas_Tpc_Monitor.rst
	Configure_WcdmaMeas_Tpc_Mpedch.rst
	Configure_WcdmaMeas_Tpc_Ulcm.rst