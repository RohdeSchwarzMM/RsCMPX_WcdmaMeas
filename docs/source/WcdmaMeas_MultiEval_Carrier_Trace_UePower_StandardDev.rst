StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:UEPower:SDEViation
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:UEPower:SDEViation

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:UEPower:SDEViation
	FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:UEPower:SDEViation



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.UePower.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: