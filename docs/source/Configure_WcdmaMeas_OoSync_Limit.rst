Limit
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:OOSYnc:LIMit:PONupper
	single: CONFigure:WCDMa:MEASurement<instance>:OOSYnc:LIMit:POFFupper
	single: CONFigure:WCDMa:MEASurement<instance>:OOSYnc:LIMit:THReshold

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:OOSYnc:LIMit:PONupper
	CONFigure:WCDMa:MEASurement<instance>:OOSYnc:LIMit:POFFupper
	CONFigure:WCDMa:MEASurement<instance>:OOSYnc:LIMit:THReshold



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.OoSync.Limit.LimitCls
	:members:
	:undoc-members:
	:noindex: