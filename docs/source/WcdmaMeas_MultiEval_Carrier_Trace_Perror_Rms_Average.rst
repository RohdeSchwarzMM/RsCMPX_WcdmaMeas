Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:PERRor[:RMS]:AVERage
	single: READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:PERRor[:RMS]:AVERage

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:PERRor[:RMS]:AVERage
	READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:PERRor[:RMS]:AVERage



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.Perror.Rms.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: