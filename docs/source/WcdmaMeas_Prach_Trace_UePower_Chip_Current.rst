Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:PRACh:TRACe:UEPower:CHIP:CURRent
	single: FETCh:WCDMa:MEASurement<instance>:PRACh:TRACe:UEPower:CHIP:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:PRACh:TRACe:UEPower:CHIP:CURRent
	FETCh:WCDMa:MEASurement<instance>:PRACh:TRACe:UEPower:CHIP:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Prach.Trace.UePower.Chip.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: