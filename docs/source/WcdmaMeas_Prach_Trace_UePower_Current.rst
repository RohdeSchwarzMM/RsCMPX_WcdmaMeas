Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:PRACh:TRACe:UEPower:CURRent
	single: FETCh:WCDMa:MEASurement<instance>:PRACh:TRACe:UEPower:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:PRACh:TRACe:UEPower:CURRent
	FETCh:WCDMa:MEASurement<instance>:PRACh:TRACe:UEPower:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Prach.Trace.UePower.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: