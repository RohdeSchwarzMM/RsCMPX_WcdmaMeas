Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:TPC:TOTal:UEPower:MAXimum
	single: READ:WCDMa:MEASurement<instance>:TPC:TOTal:UEPower:MAXimum

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:TPC:TOTal:UEPower:MAXimum
	READ:WCDMa:MEASurement<instance>:TPC:TOTal:UEPower:MAXimum



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Tpc.Total.UePower.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: