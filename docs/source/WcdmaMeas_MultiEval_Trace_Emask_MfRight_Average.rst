Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:MFRight:AVERage
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:MFRight:AVERage

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:MFRight:AVERage
	FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:MFRight:AVERage



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.Emask.MfRight.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: