Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:KFILter:AVERage
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:KFILter:AVERage

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:KFILter:AVERage
	FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:KFILter:AVERage



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.Emask.Kfilter.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: