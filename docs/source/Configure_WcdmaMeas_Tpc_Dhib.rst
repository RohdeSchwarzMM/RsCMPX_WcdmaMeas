Dhib
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:TPC:DHIB:MLENgth
	single: CONFigure:WCDMa:MEASurement<instance>:TPC:DHIB:PATTern
	single: CONFigure:WCDMa:MEASurement<instance>:TPC:DHIB:AEXecution

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:TPC:DHIB:MLENgth
	CONFigure:WCDMa:MEASurement<instance>:TPC:DHIB:PATTern
	CONFigure:WCDMa:MEASurement<instance>:TPC:DHIB:AEXecution



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Tpc.Dhib.DhibCls
	:members:
	:undoc-members:
	:noindex: