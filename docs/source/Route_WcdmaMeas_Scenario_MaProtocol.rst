MaProtocol
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:WCDMa:MEASurement<instance>:SCENario:MAPRotocol

.. code-block:: python

	ROUTe:WCDMa:MEASurement<instance>:SCENario:MAPRotocol



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Route.WcdmaMeas.Scenario.MaProtocol.MaProtocolCls
	:members:
	:undoc-members:
	:noindex: