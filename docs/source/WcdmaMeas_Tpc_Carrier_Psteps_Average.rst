Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:PSTeps:AVERage
	single: FETCh:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:PSTeps:AVERage
	single: CALCulate:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:PSTeps:AVERage

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:PSTeps:AVERage
	FETCh:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:PSTeps:AVERage
	CALCulate:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:PSTeps:AVERage



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Tpc.Carrier.Psteps.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: