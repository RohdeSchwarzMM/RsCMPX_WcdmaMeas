CdPower
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.CdPower.CdPowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.listPy.cdPower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_ListPy_CdPower_Average.rst
	WcdmaMeas_MultiEval_ListPy_CdPower_Current.rst
	WcdmaMeas_MultiEval_ListPy_CdPower_Dpcch.rst
	WcdmaMeas_MultiEval_ListPy_CdPower_Dpdch.rst
	WcdmaMeas_MultiEval_ListPy_CdPower_Edpcch.rst
	WcdmaMeas_MultiEval_ListPy_CdPower_Edpdch.rst
	WcdmaMeas_MultiEval_ListPy_CdPower_Hsdpcch.rst
	WcdmaMeas_MultiEval_ListPy_CdPower_Maximum.rst
	WcdmaMeas_MultiEval_ListPy_CdPower_Minimum.rst
	WcdmaMeas_MultiEval_ListPy_CdPower_StandardDev.rst