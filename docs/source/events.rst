RsCMPX_WcdmaMeas Events
==========================

.. _Events:

Check the usage in the Getting Started chapter :ref:`here <GetingStarted_Events>`.

.. autoclass:: RsCMPX_WcdmaMeas.CustomFiles.events.Events()
   :members:
   :undoc-members:
   :special-members: enable_properties
   :noindex:
