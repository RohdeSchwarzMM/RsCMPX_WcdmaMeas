Minimumc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALCulate:WCDMa:MEASurement<instance>:TPC:DHIB:MINimumc

.. code-block:: python

	CALCulate:WCDMa:MEASurement<instance>:TPC:DHIB:MINimumc



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Tpc.Dhib.Minimumc.MinimumcCls
	:members:
	:undoc-members:
	:noindex: