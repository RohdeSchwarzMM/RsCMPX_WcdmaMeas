Preamble<Preamble>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr5
	rc = driver.wcdmaMeas.prach.preamble.repcap_preamble_get()
	driver.wcdmaMeas.prach.preamble.repcap_preamble_set(repcap.Preamble.Nr1)





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Prach.Preamble.PreambleCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.prach.preamble.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_Prach_Preamble_Current.rst