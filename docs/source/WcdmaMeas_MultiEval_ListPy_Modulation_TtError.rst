TtError
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.Modulation.TtError.TtErrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.listPy.modulation.ttError.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_ListPy_Modulation_TtError_Average.rst
	WcdmaMeas_MultiEval_ListPy_Modulation_TtError_Current.rst
	WcdmaMeas_MultiEval_ListPy_Modulation_TtError_Maximum.rst
	WcdmaMeas_MultiEval_ListPy_Modulation_TtError_StandardDev.rst