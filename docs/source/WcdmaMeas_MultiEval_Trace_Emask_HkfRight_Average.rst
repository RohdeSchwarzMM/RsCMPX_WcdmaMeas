Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:HKFRight:AVERage
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:HKFRight:AVERage

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:HKFRight:AVERage
	FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:HKFRight:AVERage



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.Emask.HkfRight.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: