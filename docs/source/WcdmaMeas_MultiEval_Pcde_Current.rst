Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:PCDE:CURRent
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:PCDE:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:PCDE:CURRent
	FETCh:WCDMa:MEASurement<instance>:MEValuation:PCDE:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Pcde.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: