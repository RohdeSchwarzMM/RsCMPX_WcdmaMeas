EpStep
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIMit:PCONtrol:EPSTep

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIMit:PCONtrol:EPSTep



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.Limit.Pcontrol.EpStep.EpStepCls
	:members:
	:undoc-members:
	:noindex: