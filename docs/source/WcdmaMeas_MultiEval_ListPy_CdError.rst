CdError
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.CdError.CdErrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.listPy.cdError.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_ListPy_CdError_Average.rst
	WcdmaMeas_MultiEval_ListPy_CdError_Current.rst
	WcdmaMeas_MultiEval_ListPy_CdError_Dpcch.rst
	WcdmaMeas_MultiEval_ListPy_CdError_Dpdch.rst
	WcdmaMeas_MultiEval_ListPy_CdError_Edpcch.rst
	WcdmaMeas_MultiEval_ListPy_CdError_Edpdch.rst
	WcdmaMeas_MultiEval_ListPy_CdError_Hsdpcch.rst
	WcdmaMeas_MultiEval_ListPy_CdError_Maximum.rst
	WcdmaMeas_MultiEval_ListPy_CdError_StandardDev.rst