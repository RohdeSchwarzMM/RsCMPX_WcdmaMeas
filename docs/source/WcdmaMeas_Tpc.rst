Tpc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: STOP:WCDMa:MEASurement<instance>:TPC
	single: ABORt:WCDMa:MEASurement<instance>:TPC
	single: INITiate:WCDMa:MEASurement<instance>:TPC

.. code-block:: python

	STOP:WCDMa:MEASurement<instance>:TPC
	ABORt:WCDMa:MEASurement<instance>:TPC
	INITiate:WCDMa:MEASurement<instance>:TPC



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Tpc.TpcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.tpc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_Tpc_Carrier.rst
	WcdmaMeas_Tpc_Dhib.rst
	WcdmaMeas_Tpc_State.rst
	WcdmaMeas_Tpc_Total.rst