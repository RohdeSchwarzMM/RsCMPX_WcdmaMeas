Emask
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.Spectrum.Emask.EmaskCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.listPy.spectrum.emask.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_ListPy_Spectrum_Emask_Ab.rst
	WcdmaMeas_MultiEval_ListPy_Spectrum_Emask_Ba.rst
	WcdmaMeas_MultiEval_ListPy_Spectrum_Emask_Bc.rst
	WcdmaMeas_MultiEval_ListPy_Spectrum_Emask_Cb.rst
	WcdmaMeas_MultiEval_ListPy_Spectrum_Emask_Cd.rst
	WcdmaMeas_MultiEval_ListPy_Spectrum_Emask_Dc.rst
	WcdmaMeas_MultiEval_ListPy_Spectrum_Emask_Ef.rst
	WcdmaMeas_MultiEval_ListPy_Spectrum_Emask_Fe.rst
	WcdmaMeas_MultiEval_ListPy_Spectrum_Emask_Had.rst
	WcdmaMeas_MultiEval_ListPy_Spectrum_Emask_Hda.rst