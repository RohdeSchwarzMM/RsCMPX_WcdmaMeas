Perror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIMit:PERRor

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIMit:PERRor



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.Limit.Perror.PerrorCls
	:members:
	:undoc-members:
	:noindex: