Modulation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIST:SEGMent<nr>:MODulation

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIST:SEGMent<nr>:MODulation



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.ListPy.Segment.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex: