Modulation
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.listPy.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_ListPy_Modulation_Average.rst
	WcdmaMeas_MultiEval_ListPy_Modulation_Current.rst
	WcdmaMeas_MultiEval_ListPy_Modulation_Evm.rst
	WcdmaMeas_MultiEval_ListPy_Modulation_FreqError.rst
	WcdmaMeas_MultiEval_ListPy_Modulation_IqImbalance.rst
	WcdmaMeas_MultiEval_ListPy_Modulation_IqOffset.rst
	WcdmaMeas_MultiEval_ListPy_Modulation_Maximum.rst
	WcdmaMeas_MultiEval_ListPy_Modulation_Merror.rst
	WcdmaMeas_MultiEval_ListPy_Modulation_Perror.rst
	WcdmaMeas_MultiEval_ListPy_Modulation_StandardDev.rst
	WcdmaMeas_MultiEval_ListPy_Modulation_TtError.rst
	WcdmaMeas_MultiEval_ListPy_Modulation_UePower.rst