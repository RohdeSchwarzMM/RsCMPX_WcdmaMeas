Dpcch
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:UECHannels:CARRier<carrier>:DPCCh

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:UECHannels:CARRier<carrier>:DPCCh



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.UeChannels.Carrier.Dpcch.DpcchCls
	:members:
	:undoc-members:
	:noindex: