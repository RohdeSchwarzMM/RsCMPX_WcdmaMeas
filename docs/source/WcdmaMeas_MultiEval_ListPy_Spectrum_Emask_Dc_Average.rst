Average
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:LIST:SPECtrum:EMASk:DC:AVERage

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:LIST:SPECtrum:EMASk:DC:AVERage



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.Spectrum.Emask.Dc.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: