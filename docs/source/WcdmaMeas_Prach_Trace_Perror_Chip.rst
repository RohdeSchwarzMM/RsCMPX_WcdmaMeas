Chip
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Prach.Trace.Perror.Chip.ChipCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.prach.trace.perror.chip.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_Prach_Trace_Perror_Chip_Current.rst