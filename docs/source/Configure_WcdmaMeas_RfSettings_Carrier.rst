Carrier<Carrier>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.configure.wcdmaMeas.rfSettings.carrier.repcap_carrier_get()
	driver.configure.wcdmaMeas.rfSettings.carrier.repcap_carrier_set(repcap.Carrier.Nr1)





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.RfSettings.Carrier.CarrierCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wcdmaMeas.rfSettings.carrier.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WcdmaMeas_RfSettings_Carrier_Frequency.rst