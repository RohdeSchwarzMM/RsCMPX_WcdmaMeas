CdThreshold
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:CDTHreshold:MODulation

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:MEValuation:CDTHreshold:MODulation



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.CdThreshold.CdThresholdCls
	:members:
	:undoc-members:
	:noindex: