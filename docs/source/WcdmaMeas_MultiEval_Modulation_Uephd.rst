Uephd
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:WCDMa:MEASurement<instance>:MEValuation:MODulation:UEPHd
	single: READ:WCDMa:MEASurement<instance>:MEValuation:MODulation:UEPHd
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:MODulation:UEPHd

.. code-block:: python

	CALCulate:WCDMa:MEASurement<instance>:MEValuation:MODulation:UEPHd
	READ:WCDMa:MEASurement<instance>:MEValuation:MODulation:UEPHd
	FETCh:WCDMa:MEASurement<instance>:MEValuation:MODulation:UEPHd



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Modulation.Uephd.UephdCls
	:members:
	:undoc-members:
	:noindex: