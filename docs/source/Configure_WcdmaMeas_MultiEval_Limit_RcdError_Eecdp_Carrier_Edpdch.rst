Edpdch<EdpdChannel>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.configure.wcdmaMeas.multiEval.limit.rcdError.eecdp.carrier.edpdch.repcap_edpdChannel_get()
	driver.configure.wcdmaMeas.multiEval.limit.rcdError.eecdp.carrier.edpdch.repcap_edpdChannel_set(repcap.EdpdChannel.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIMit:RCDerror:EECDp:CARRier<carrier>:EDPDch<nr>

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIMit:RCDerror:EECDp:CARRier<carrier>:EDPDch<nr>



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.Limit.RcdError.Eecdp.Carrier.Edpdch.EdpdchCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wcdmaMeas.multiEval.limit.rcdError.eecdp.carrier.edpdch.clone()