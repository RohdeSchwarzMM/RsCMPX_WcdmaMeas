Trace
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_Trace_Carrier.rst
	WcdmaMeas_MultiEval_Trace_CdeMonitor.rst
	WcdmaMeas_MultiEval_Trace_CdpMonitor.rst
	WcdmaMeas_MultiEval_Trace_Emask.rst
	WcdmaMeas_MultiEval_Trace_EvMagnitude.rst
	WcdmaMeas_MultiEval_Trace_Iq.rst
	WcdmaMeas_MultiEval_Trace_Merror.rst
	WcdmaMeas_MultiEval_Trace_Perror.rst
	WcdmaMeas_MultiEval_Trace_Phd.rst