Result
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:PRACh:RESult:UEPower
	single: CONFigure:WCDMa:MEASurement<instance>:PRACh:RESult:PSTeps
	single: CONFigure:WCDMa:MEASurement<instance>:PRACh:RESult:FERRor
	single: CONFigure:WCDMa:MEASurement<instance>:PRACh:RESult[:ALL]
	single: CONFigure:WCDMa:MEASurement<instance>:PRACh:RESult:PERRor
	single: CONFigure:WCDMa:MEASurement<instance>:PRACh:RESult:EVMagnitude
	single: CONFigure:WCDMa:MEASurement<instance>:PRACh:RESult:MERRor
	single: CONFigure:WCDMa:MEASurement<instance>:PRACh:RESult:IQ

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:PRACh:RESult:UEPower
	CONFigure:WCDMa:MEASurement<instance>:PRACh:RESult:PSTeps
	CONFigure:WCDMa:MEASurement<instance>:PRACh:RESult:FERRor
	CONFigure:WCDMa:MEASurement<instance>:PRACh:RESult[:ALL]
	CONFigure:WCDMa:MEASurement<instance>:PRACh:RESult:PERRor
	CONFigure:WCDMa:MEASurement<instance>:PRACh:RESult:EVMagnitude
	CONFigure:WCDMa:MEASurement<instance>:PRACh:RESult:MERRor
	CONFigure:WCDMa:MEASurement<instance>:PRACh:RESult:IQ



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Prach.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wcdmaMeas.prach.result.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WcdmaMeas_Prach_Result_Chip.rst