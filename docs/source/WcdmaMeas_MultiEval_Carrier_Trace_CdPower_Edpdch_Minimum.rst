Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:CDPower:EDPDch<nr>:MINimum
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:CDPower:EDPDch<nr>:MINimum

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:CDPower:EDPDch<nr>:MINimum
	FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:CDPower:EDPDch<nr>:MINimum



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.CdPower.Edpdch.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: