CdError
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.CdError.CdErrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.carrier.cdError.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_Carrier_CdError_Average.rst
	WcdmaMeas_MultiEval_Carrier_CdError_Current.rst
	WcdmaMeas_MultiEval_Carrier_CdError_Maximum.rst
	WcdmaMeas_MultiEval_Carrier_CdError_StandardDev.rst