Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:CDEMonitor:ISIGnal:CURRent
	single: READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:CDEMonitor:ISIGnal:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:CDEMonitor:ISIGnal:CURRent
	READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:CDEMonitor:ISIGnal:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.CdeMonitor.Isignal.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: