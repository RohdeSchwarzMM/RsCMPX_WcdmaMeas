CdError
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.CdError.CdErrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.carrier.trace.cdError.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_Carrier_Trace_CdError_Dpcch.rst
	WcdmaMeas_MultiEval_Carrier_Trace_CdError_Dpdch.rst
	WcdmaMeas_MultiEval_Carrier_Trace_CdError_Edpcch.rst
	WcdmaMeas_MultiEval_Carrier_Trace_CdError_Edpdch.rst
	WcdmaMeas_MultiEval_Carrier_Trace_CdError_Hsdpcch.rst