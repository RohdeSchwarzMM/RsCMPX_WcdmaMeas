Config
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:UECHannels:CARRier<carrier>:HSDPcch:CONFig

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:UECHannels:CARRier<carrier>:HSDPcch:CONFig



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.UeChannels.Carrier.Hsdpcch.Config.ConfigCls
	:members:
	:undoc-members:
	:noindex: