Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:MFLeft:AVERage
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:MFLeft:AVERage

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:MFLeft:AVERage
	FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:MFLeft:AVERage



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.Emask.MfLeft.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: