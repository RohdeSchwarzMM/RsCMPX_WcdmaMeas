UePower
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIST:SEGMent<nr>:UEPower

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIST:SEGMent<nr>:UEPower



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.ListPy.Segment.UePower.UePowerCls
	:members:
	:undoc-members:
	:noindex: