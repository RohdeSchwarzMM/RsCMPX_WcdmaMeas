Minimum
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:LIST:CDPower:MINimum

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:LIST:CDPower:MINimum



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.CdPower.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: