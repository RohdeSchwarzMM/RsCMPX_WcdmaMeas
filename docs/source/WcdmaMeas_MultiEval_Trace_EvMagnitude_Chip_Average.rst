Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EVMagnitude:CHIP:AVERage
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EVMagnitude:CHIP:AVERage

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EVMagnitude:CHIP:AVERage
	FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EVMagnitude:CHIP:AVERage



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.EvMagnitude.Chip.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: