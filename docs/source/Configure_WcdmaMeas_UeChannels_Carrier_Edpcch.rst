Edpcch
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:UECHannels:CARRier<carrier>:EDPCch

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:UECHannels:CARRier<carrier>:EDPCch



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.UeChannels.Carrier.Edpcch.EdpcchCls
	:members:
	:undoc-members:
	:noindex: