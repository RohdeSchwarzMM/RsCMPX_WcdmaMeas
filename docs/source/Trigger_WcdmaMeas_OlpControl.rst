OlpControl
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:WCDMa:MEASurement<instance>:OLPControl:DELay
	single: TRIGger:WCDMa:MEASurement<instance>:OLPControl:MGAP
	single: TRIGger:WCDMa:MEASurement<instance>:OLPControl:THReshold
	single: TRIGger:WCDMa:MEASurement<instance>:OLPControl:SLOPe
	single: TRIGger:WCDMa:MEASurement<instance>:OLPControl:TOUT
	single: TRIGger:WCDMa:MEASurement<instance>:OLPControl:SOURce

.. code-block:: python

	TRIGger:WCDMa:MEASurement<instance>:OLPControl:DELay
	TRIGger:WCDMa:MEASurement<instance>:OLPControl:MGAP
	TRIGger:WCDMa:MEASurement<instance>:OLPControl:THReshold
	TRIGger:WCDMa:MEASurement<instance>:OLPControl:SLOPe
	TRIGger:WCDMa:MEASurement<instance>:OLPControl:TOUT
	TRIGger:WCDMa:MEASurement<instance>:OLPControl:SOURce



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Trigger.WcdmaMeas.OlpControl.OlpControlCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.wcdmaMeas.olpControl.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_WcdmaMeas_OlpControl_Catalog.rst