Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:TRACe:PSTeps:CURRent
	single: READ:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:TRACe:PSTeps:CURRent
	single: CALCulate:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:TRACe:PSTeps:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:TRACe:PSTeps:CURRent
	READ:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:TRACe:PSTeps:CURRent
	CALCulate:WCDMa:MEASurement<instance>:TPC:CARRier<carrier>:TRACe:PSTeps:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Tpc.Carrier.Trace.Psteps.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: