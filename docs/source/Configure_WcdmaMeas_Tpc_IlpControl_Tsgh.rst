Tsgh
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:TPC:ILPControl:TSGH

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:TPC:ILPControl:TSGH



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Tpc.IlpControl.Tsgh.TsghCls
	:members:
	:undoc-members:
	:noindex: