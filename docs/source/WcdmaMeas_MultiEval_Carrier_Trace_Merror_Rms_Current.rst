Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:MERRor[:RMS]:CURRent
	single: READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:MERRor[:RMS]:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:MERRor[:RMS]:CURRent
	READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:MERRor[:RMS]:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.Merror.Rms.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: