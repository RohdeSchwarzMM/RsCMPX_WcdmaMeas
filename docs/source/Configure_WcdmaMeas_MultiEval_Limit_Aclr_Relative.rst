Relative
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIMit:ACLR:RELative

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIMit:ACLR:RELative



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.Limit.Aclr.Relative.RelativeCls
	:members:
	:undoc-members:
	:noindex: