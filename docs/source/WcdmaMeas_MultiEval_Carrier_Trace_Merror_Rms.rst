Rms
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.Merror.Rms.RmsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.carrier.trace.merror.rms.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_Carrier_Trace_Merror_Rms_Average.rst
	WcdmaMeas_MultiEval_Carrier_Trace_Merror_Rms_Current.rst
	WcdmaMeas_MultiEval_Carrier_Trace_Merror_Rms_Maximum.rst
	WcdmaMeas_MultiEval_Carrier_Trace_Merror_Rms_StandardDev.rst