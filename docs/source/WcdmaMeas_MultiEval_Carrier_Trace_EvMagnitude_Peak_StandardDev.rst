StandardDev
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:EVMagnitude:PEAK:SDEViation

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:EVMagnitude:PEAK:SDEViation



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.EvMagnitude.Peak.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: