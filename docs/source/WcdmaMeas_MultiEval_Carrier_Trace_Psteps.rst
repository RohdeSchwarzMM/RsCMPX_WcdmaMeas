Psteps
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.Psteps.PstepsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.carrier.trace.psteps.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_Carrier_Trace_Psteps_Average.rst
	WcdmaMeas_MultiEval_Carrier_Trace_Psteps_Current.rst
	WcdmaMeas_MultiEval_Carrier_Trace_Psteps_Maximum.rst
	WcdmaMeas_MultiEval_Carrier_Trace_Psteps_Minimum.rst
	WcdmaMeas_MultiEval_Carrier_Trace_Psteps_StandardDev.rst