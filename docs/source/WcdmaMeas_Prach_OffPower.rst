OffPower
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:PRACh:OFFPower
	single: READ:WCDMa:MEASurement<instance>:PRACh:OFFPower
	single: CALCulate:WCDMa:MEASurement<instance>:PRACh:OFFPower

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:PRACh:OFFPower
	READ:WCDMa:MEASurement<instance>:PRACh:OFFPower
	CALCulate:WCDMa:MEASurement<instance>:PRACh:OFFPower



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Prach.OffPower.OffPowerCls
	:members:
	:undoc-members:
	:noindex: