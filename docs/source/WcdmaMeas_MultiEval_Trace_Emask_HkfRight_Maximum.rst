Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:HKFRight:MAXimum
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:HKFRight:MAXimum

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:HKFRight:MAXimum
	FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:HKFRight:MAXimum



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.Emask.HkfRight.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: