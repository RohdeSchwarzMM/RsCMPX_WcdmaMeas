ListPy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIST:EOFFset
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIST:COUNt
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIST:OSINdex
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIST:CMODe
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIST:NCONnections
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIST

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIST:EOFFset
	CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIST:COUNt
	CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIST:OSINdex
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIST:CMODe
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIST:NCONnections
	CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIST



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.ListPy.ListPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wcdmaMeas.multiEval.listPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WcdmaMeas_MultiEval_ListPy_Segment.rst
	Configure_WcdmaMeas_MultiEval_ListPy_SingleCmw.rst