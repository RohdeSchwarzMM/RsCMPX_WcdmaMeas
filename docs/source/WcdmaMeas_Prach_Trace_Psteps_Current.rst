Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:PRACh:TRACe:PSTeps:CURRent
	single: FETCh:WCDMa:MEASurement<instance>:PRACh:TRACe:PSTeps:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:PRACh:TRACe:PSTeps:CURRent
	FETCh:WCDMa:MEASurement<instance>:PRACh:TRACe:PSTeps:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Prach.Trace.Psteps.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: