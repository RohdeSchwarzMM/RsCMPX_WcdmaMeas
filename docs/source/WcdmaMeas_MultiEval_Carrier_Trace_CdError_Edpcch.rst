Edpcch
----------------------------------------





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.CdError.Edpcch.EdpcchCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.multiEval.carrier.trace.cdError.edpcch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_MultiEval_Carrier_Trace_CdError_Edpcch_Average.rst
	WcdmaMeas_MultiEval_Carrier_Trace_CdError_Edpcch_Current.rst
	WcdmaMeas_MultiEval_Carrier_Trace_CdError_Edpcch_Maximum.rst
	WcdmaMeas_MultiEval_Carrier_Trace_CdError_Edpcch_StandardDev.rst