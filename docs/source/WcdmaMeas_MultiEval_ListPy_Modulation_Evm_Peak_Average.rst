Average
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:LIST:MODulation:EVM:PEAK:AVERage

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:LIST:MODulation:EVM:PEAK:AVERage



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.Modulation.Evm.Peak.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: