Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:MFLeft:MAXimum
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:MFLeft:MAXimum

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:MFLeft:MAXimum
	FETCh:WCDMa:MEASurement<instance>:MEValuation:TRACe:EMASk:MFLeft:MAXimum



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.Emask.MfLeft.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: