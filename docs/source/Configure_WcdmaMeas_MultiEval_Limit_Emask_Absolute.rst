Absolute
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIMit:EMASk:ABSolute

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIMit:EMASk:ABSolute



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.Limit.Emask.Absolute.AbsoluteCls
	:members:
	:undoc-members:
	:noindex: