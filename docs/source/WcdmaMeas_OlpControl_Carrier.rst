Carrier<CARRierExt>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr32
	rc = driver.wcdmaMeas.olpControl.carrier.repcap_cARRierExt_get()
	driver.wcdmaMeas.olpControl.carrier.repcap_cARRierExt_set(repcap.CARRierExt.Nr1)





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.OlpControl.Carrier.CarrierCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.olpControl.carrier.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_OlpControl_Carrier_UepPower.rst