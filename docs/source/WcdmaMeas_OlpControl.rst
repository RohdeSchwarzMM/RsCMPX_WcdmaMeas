OlpControl
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: STOP:WCDMa:MEASurement<instance>:OLPControl
	single: ABORt:WCDMa:MEASurement<instance>:OLPControl
	single: INITiate:WCDMa:MEASurement<instance>:OLPControl
	single: READ:WCDMa:MEASurement<instance>:OLPControl
	single: FETCh:WCDMa:MEASurement<instance>:OLPControl
	single: CALCulate:WCDMa:MEASurement<instance>:OLPControl

.. code-block:: python

	STOP:WCDMa:MEASurement<instance>:OLPControl
	ABORt:WCDMa:MEASurement<instance>:OLPControl
	INITiate:WCDMa:MEASurement<instance>:OLPControl
	READ:WCDMa:MEASurement<instance>:OLPControl
	FETCh:WCDMa:MEASurement<instance>:OLPControl
	CALCulate:WCDMa:MEASurement<instance>:OLPControl



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.OlpControl.OlpControlCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wcdmaMeas.olpControl.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WcdmaMeas_OlpControl_Carrier.rst
	WcdmaMeas_OlpControl_State.rst