Current
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:CARRier<carrier>:PERRor[:RMS]:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:TRACe:CARRier<carrier>:PERRor[:RMS]:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Trace.Carrier.Perror.Rms.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: