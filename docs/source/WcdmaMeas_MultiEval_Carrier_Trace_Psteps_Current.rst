Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:PSTeps:CURRent
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:PSTeps:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:PSTeps:CURRent
	FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:TRACe:PSTeps:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.Trace.Psteps.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: