Dcarrier
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:RFSettings:DCARrier:SEParation

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:RFSettings:DCARrier:SEParation



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.RfSettings.Dcarrier.DcarrierCls
	:members:
	:undoc-members:
	:noindex: