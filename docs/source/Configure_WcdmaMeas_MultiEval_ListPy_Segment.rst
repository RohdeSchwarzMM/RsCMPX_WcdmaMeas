Segment<Segment>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr200
	rc = driver.configure.wcdmaMeas.multiEval.listPy.segment.repcap_segment_get()
	driver.configure.wcdmaMeas.multiEval.listPy.segment.repcap_segment_set(repcap.Segment.Nr1)





.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.ListPy.Segment.SegmentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wcdmaMeas.multiEval.listPy.segment.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WcdmaMeas_MultiEval_ListPy_Segment_CdPower.rst
	Configure_WcdmaMeas_MultiEval_ListPy_Segment_Cidx.rst
	Configure_WcdmaMeas_MultiEval_ListPy_Segment_Modulation.rst
	Configure_WcdmaMeas_MultiEval_ListPy_Segment_Phd.rst
	Configure_WcdmaMeas_MultiEval_ListPy_Segment_Setup.rst
	Configure_WcdmaMeas_MultiEval_ListPy_Segment_SingleCmw.rst
	Configure_WcdmaMeas_MultiEval_ListPy_Segment_Spectrum.rst
	Configure_WcdmaMeas_MultiEval_ListPy_Segment_UePower.rst