Amode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:AMODe:MODulation

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:MEValuation:AMODe:MODulation



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.Amode.AmodeCls
	:members:
	:undoc-members:
	:noindex: