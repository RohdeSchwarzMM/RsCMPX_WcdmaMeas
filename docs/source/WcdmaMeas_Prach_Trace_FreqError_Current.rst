Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:PRACh:TRACe:FERRor:CURRent
	single: FETCh:WCDMa:MEASurement<instance>:PRACh:TRACe:FERRor:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:PRACh:TRACe:FERRor:CURRent
	FETCh:WCDMa:MEASurement<instance>:PRACh:TRACe:FERRor:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Prach.Trace.FreqError.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: