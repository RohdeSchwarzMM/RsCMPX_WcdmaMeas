Monitor
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:TPC:MONitor:MLENgth

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:TPC:MONitor:MLENgth



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Tpc.Monitor.MonitorCls
	:members:
	:undoc-members:
	:noindex: