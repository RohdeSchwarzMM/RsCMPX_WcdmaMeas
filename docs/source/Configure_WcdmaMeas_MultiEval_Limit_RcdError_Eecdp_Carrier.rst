Carrier<Carrier>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.configure.wcdmaMeas.multiEval.limit.rcdError.eecdp.carrier.repcap_carrier_get()
	driver.configure.wcdmaMeas.multiEval.limit.rcdError.eecdp.carrier.repcap_carrier_set(repcap.Carrier.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIMit:RCDerror:EECDp:CARRier<carrier>

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:MEValuation:LIMit:RCDerror:EECDp:CARRier<carrier>



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.Limit.RcdError.Eecdp.Carrier.CarrierCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wcdmaMeas.multiEval.limit.rcdError.eecdp.carrier.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WcdmaMeas_MultiEval_Limit_RcdError_Eecdp_Carrier_Dpcch.rst
	Configure_WcdmaMeas_MultiEval_Limit_RcdError_Eecdp_Carrier_Dpdch.rst
	Configure_WcdmaMeas_MultiEval_Limit_RcdError_Eecdp_Carrier_Edpcch.rst
	Configure_WcdmaMeas_MultiEval_Limit_RcdError_Eecdp_Carrier_Edpdch.rst
	Configure_WcdmaMeas_MultiEval_Limit_RcdError_Eecdp_Carrier_Hsdpcch.rst