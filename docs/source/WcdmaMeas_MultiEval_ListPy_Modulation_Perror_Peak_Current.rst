Current
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:LIST:MODulation:PERRor:PEAK:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:MEValuation:LIST:MODulation:PERRor:PEAK:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.ListPy.Modulation.Perror.Peak.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: