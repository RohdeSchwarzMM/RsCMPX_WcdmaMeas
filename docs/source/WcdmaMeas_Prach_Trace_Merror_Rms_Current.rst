Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:WCDMa:MEASurement<instance>:PRACh:TRACe:MERRor[:RMS]:CURRent
	single: READ:WCDMa:MEASurement<instance>:PRACh:TRACe:MERRor[:RMS]:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<instance>:PRACh:TRACe:MERRor[:RMS]:CURRent
	READ:WCDMa:MEASurement<instance>:PRACh:TRACe:MERRor[:RMS]:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Prach.Trace.Merror.Rms.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: