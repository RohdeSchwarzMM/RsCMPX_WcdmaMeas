MultiEval
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:TOUT
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:MSCount
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:PSLot
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:SYNCh
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:MOEXception
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:SCONdition
	single: CONFigure:WCDMa:MEASurement<instance>:MEValuation:REPetition

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:MEValuation:TOUT
	CONFigure:WCDMa:MEASurement<instance>:MEValuation:MSCount
	CONFigure:WCDMa:MEASurement<instance>:MEValuation:PSLot
	CONFigure:WCDMa:MEASurement<instance>:MEValuation:SYNCh
	CONFigure:WCDMa:MEASurement<instance>:MEValuation:MOEXception
	CONFigure:WCDMa:MEASurement<instance>:MEValuation:SCONdition
	CONFigure:WCDMa:MEASurement<instance>:MEValuation:REPetition



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wcdmaMeas.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WcdmaMeas_MultiEval_Amode.rst
	Configure_WcdmaMeas_MultiEval_CdThreshold.rst
	Configure_WcdmaMeas_MultiEval_Dmode.rst
	Configure_WcdmaMeas_MultiEval_DsFactor.rst
	Configure_WcdmaMeas_MultiEval_Limit.rst
	Configure_WcdmaMeas_MultiEval_ListPy.rst
	Configure_WcdmaMeas_MultiEval_Mperiod.rst
	Configure_WcdmaMeas_MultiEval_Result.rst
	Configure_WcdmaMeas_MultiEval_Rotation.rst
	Configure_WcdmaMeas_MultiEval_Scount.rst
	Configure_WcdmaMeas_MultiEval_Sscalar.rst