Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:RCDerror:AVERage
	single: FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:RCDerror:AVERage
	single: READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:RCDerror:AVERage

.. code-block:: python

	CALCulate:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:RCDerror:AVERage
	FETCh:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:RCDerror:AVERage
	READ:WCDMa:MEASurement<instance>:MEValuation:CARRier<carrier>:RCDerror:AVERage



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.MultiEval.Carrier.RcdError.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: