Pb
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WCDMa:MEASurement<instance>:TPC:LIMit:ULCM:PB

.. code-block:: python

	CONFigure:WCDMa:MEASurement<instance>:TPC:LIMit:ULCM:PB



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Configure.WcdmaMeas.Tpc.Limit.Ulcm.Pb.PbCls
	:members:
	:undoc-members:
	:noindex: