MultiEval
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:WCDMa:MEASurement<instance>:MEValuation:DELay
	single: TRIGger:WCDMa:MEASurement<instance>:MEValuation:MGAP
	single: TRIGger:WCDMa:MEASurement<instance>:MEValuation:THReshold
	single: TRIGger:WCDMa:MEASurement<instance>:MEValuation:SLOPe
	single: TRIGger:WCDMa:MEASurement<instance>:MEValuation:TOUT
	single: TRIGger:WCDMa:MEASurement<instance>:MEValuation:SOURce

.. code-block:: python

	TRIGger:WCDMa:MEASurement<instance>:MEValuation:DELay
	TRIGger:WCDMa:MEASurement<instance>:MEValuation:MGAP
	TRIGger:WCDMa:MEASurement<instance>:MEValuation:THReshold
	TRIGger:WCDMa:MEASurement<instance>:MEValuation:SLOPe
	TRIGger:WCDMa:MEASurement<instance>:MEValuation:TOUT
	TRIGger:WCDMa:MEASurement<instance>:MEValuation:SOURce



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.Trigger.WcdmaMeas.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.wcdmaMeas.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_WcdmaMeas_MultiEval_Catalog.rst
	Trigger_WcdmaMeas_MultiEval_ListPy.rst