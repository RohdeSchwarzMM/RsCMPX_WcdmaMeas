Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WCDMa:MEASurement<instance>:PRACh:PREamble<nr>:CURRent
	single: FETCh:WCDMa:MEASurement<instance>:PRACh:PREamble<nr>:CURRent
	single: CALCulate:WCDMa:MEASurement<instance>:PRACh:PREamble<nr>:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<instance>:PRACh:PREamble<nr>:CURRent
	FETCh:WCDMa:MEASurement<instance>:PRACh:PREamble<nr>:CURRent
	CALCulate:WCDMa:MEASurement<instance>:PRACh:PREamble<nr>:CURRent



.. autoclass:: RsCMPX_WcdmaMeas.Implementations.WcdmaMeas.Prach.Preamble.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: